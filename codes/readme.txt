# RKI Data
The RKI data is downloaded regularly from "https://github.com/ard-data/2020-rki-archive/"
For updating the dataset, the R script in ./data/ard_archive.R shall be called.
For the first run, the variable "cloning" needs to be TRUE, and then FALSE for later updates.