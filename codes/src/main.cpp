#include <iostream>
#include <sys/stat.h> // for making directories
#include <string.h>
#include <fstream>
#include "general_functions.h"
#include "models.h"
#include <omp.h>
#include <chrono> 
#include <boost/numeric/odeint.hpp>
#include <vector>

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
std::vector<std::string> cmdArg; // Command-line arguments
int numThreads = omp_get_max_threads();
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
using namespace boost::numeric::odeint;
using namespace boost;
using namespace std;
using namespace std::chrono;

//-------------------------------------------------------------------------
//---------- Function prototypes ------------------------------------------
//-------------------------------------------------------------------------
void help();

//-------------------------------------------------------------------------
//------------- Gateway functions -----------------------------------------
//-------------------------------------------------------------------------
int main(int argc, char **argv){
    bool Report			= true;
	bool dead_fitting	= false;
	bool single_run		= false;
	bool pool_run		= false;
	bool prediction		= false;	// Given a session name, an ending date, 
									// duration and parameter settings,
									// the simulation is continued with last state
									// values

	double Population = 0;

	std::string mode;
    std::string outputFile;
    std::string cityName;
	std::string dataFileNameAddress;
	std::string sessionName;
	std::string scenarioName = "";
	std::string parameterFile = "";

	int numOfRuns = 100;
    //-------------------------------------------------------------------------
    if (argc > 1){ // Check if help() is requested or no argument is provided.
        if (boost::iequals(argv[1], "?")){
            help();
            return 0;
        }
    }
    else{
        help();
        return 0;
    }
    if (argc > 2){
        int k = 1;
        sessionName = argv[k]; k++;
        cityName    = argv[k]; k++;

		

        if (argc > 1){ // The remaining cmdArgumentd must be parameter restrictions, which are then saved for further consideration.
            for (int i = k; i < argc; i++)
                cmdArg.push_back(argv[i]);
        }
    }
    else{
        std::cout << "main.cpp: More arguments should be provided." << std::endl;
        help();
        return 0;
    }
    //-------------------------------------------------------------------------
    // Processing command-line arguments
    //-------------------------------------------------------------------------
    for (int i=0; i < cmdArg.size();i++){
		if (cmdArg[i] == "mode")			mode = cmdArg[i + 1];
		if (cmdArg[i] == "scenarioName")	scenarioName = cmdArg[i + 1];
		if (cmdArg[i] == "iterations")		numOfRuns = stoi(cmdArg[i + 1]);
		if (cmdArg[i] == "Population")		Population = stod(cmdArg[i + 1]);
		if (cmdArg[i] == "numThreads"){
			numThreads = stoi(cmdArg[i + 1]);
			omp_set_num_threads(numThreads);
		}
		if (cmdArg[i] == "parameterFile") {
			parameterFile = cmdArg[i + 1];
			if (!fileExists(parameterFile)) {
				std::cout << "Err: The file containing parameters (" << parameterFile << ") is not found!\n";
				return 1;
			}
		}
    }

	if (mode == "dead_fitting")		dead_fitting = true;
	if (mode == "single_run")		single_run = true;
	if (mode == "pool_run")			pool_run = true;
	if (single_run)					pool_run = false;

	std::cout << "main: sessionName:   " << sessionName << std::endl;
	std::cout << "main: cityName:      " << cityName << std::endl;
	std::cout << "main: mode:          " << mode			<< std::endl;
	std::cout << "main: scenarioName:  " << scenarioName	<< std::endl;
	std::cout << "main: cityName:      " << cityName		<< std::endl;
	std::cout << "main: parameterFile: " << parameterFile	<< std::endl;

	makeDir(current_dir() + "/results/" + sessionName);

//-------------------------------------
// Pool run
//-------------------------------------
if (pool_run) {	
	// Loading data from file
	dataFileNameAddress = current_dir() + "/data/" + sessionName + "/" + cityName + ".csv";
	std::cout << "main: dataAdress:     " << dataFileNameAddress << std::endl;
	if (!fileExists(dataFileNameAddress)) {
		std::cout << "Err: The file containing data (" << dataFileNameAddress << ") is not found!\n";
		return 0;
	}
	//-------------------------------------
	iData dataset;
	dataset.load_data_from_file(dataFileNameAddress);
	dataset.N0 = Population;
	dataset.print_data();

	parameter_pool parPool;
	parPool.load_pool_from_file(parameterFile); // E.g. "./settings/param_random.csv"
	parPool.print_pool();
	
	std::vector<windowEval*> evals;// (100, windowEval(dataset, parPool.params[0]));
	for (int i = 0; i < numOfRuns; i++) {
		evals.push_back(new windowEval(dataset, parPool.params[i]));
		evals[i]->report_to_file = true;
		evals[i]->sessionName    = sessionName;
		evals[i]->scenarioName   = scenarioName;
		evals[i]->cityName       = cityName;
		evals[i]->tag = "iter_" + std::to_string(i);
	}
	
	#pragma omp parallel
	{		
	#pragma omp for
		for (int i = 0; i < numOfRuns; i++) {
			//evals[i]->param_org = parPool.params[i];			
			evals[i]->fit_run();			
		}
	}
	for (int i = 0; i < numOfRuns; i++) {
		evals[i]->write_to_file();
	}	
}

return 0;
}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

void help(){
    std::cout << std::endl;
    std::cout << "Simulation of the SECIR (revised) model. Date: 2020-12-02" << std::endl;	
    std::cout << std::endl;
}