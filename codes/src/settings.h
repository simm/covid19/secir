#ifndef SETTINGS_H
#define SETTINGS_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <sstream>      // std::stringstream

class settings
{
public:
    std::vector<std::string> paramTags;
    std::vector<double> paramMax;
    std::vector<double> paramMin;
    std::vector<bool>   paramMaxFlex;
    std::vector<bool>   paramMinFlex;

    ~settings(){}
    settings(std::string _fileAddress, std::string _fileName);
    void print_parameters();
    void processCmd(std::vector<std::string> cmdArg);

private:
    std::string fileNameAddress;
    void loadParametersTag();
    std::vector<std::vector<std::string>> lines;
    int lineSize;
};

#endif // SETTINGS_H
