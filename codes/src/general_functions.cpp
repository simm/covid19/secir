#include "general_functions.h"

//---------------------------------------------------------------------
//---------------------------------------------------------------------
std::string current_dir(){
    char *path = nullptr;
    path = getcwd(nullptr, 0); // or _getcwd
    std::string s(path);
    return s;
}

//---------------------------------------------------------------------
//---------------------------------------------------------------------

void quickSort(double *arr, int * order, int left, int right){
    int i = left, j = right;
    int tmp;
    double pivot = arr[order[(left + right) / 2]];

    /* partition */
    while (i <= j) {
        while (arr[order[i]] < pivot)
            i++;
        while (arr[order[j]] > pivot)
            j--;
        if (i <= j) {
            tmp = order[i];
            order[i] = order[j];
            order[j] = tmp;
            i++;
            j--;
        }
    }
    /* recursion */
    if (left < j)
        quickSort(arr, order, left, j);
    if (i < right)
        quickSort(arr, order, i, right);
}

//---------------------------------------------------------------------
//---------------------------------------------------------------------

bool makeDir(std::string sessionAddress){
    bool rtn = false; // rtn = return condition
    // Make a directory if it is not existing
    struct stat st;
    if(stat(sessionAddress.c_str(),&st) == 0){
        std::cout << "Warning - makeDir: folder \" " << sessionAddress.c_str() << " \" already exists. Files will be overwrited." << std::endl;
    }
    else{
        //int mkstatus = mkdir(sessionAddress.c_str(), ACCESSPERMS );
        #if defined(_WIN32)
          int mkstatus = _mkdir(sessionAddress.c_str()); // can be used on Windows
        #else
          int mkstatus = mkdir(sessionAddress.c_str(), ACCESSPERMS); // can be used on non-Windows
        #endif
        if (mkstatus != 0){
            std::cout << "Error - makeDir: cannot make folder \" " << sessionAddress.c_str() << " \"" << std::endl;
            rtn = true;
        }
        else{
            //std::cout << "makeDir: folder \" " << sessionAddress.c_str() << " \" is created." << std::endl;
        }
    }
    return rtn;
}

//---------------------------------------------------------------------
//---------------------------------------------------------------------

bool fileExists (const std::string& name){
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

//---------------------------------------------------------------------
//---------------------------------------------------------------------

/*
* Parses through csv file line by line and returns the data
* in vector of vector of strings.
*/
std::vector<std::vector<std::string> > CSVReader::getData()
{
    std::ifstream file(fileName);

    std::vector<std::vector<std::string> > dataList;

    std::string line = "";
    // Iterate through each line and split the content using delimeter
    while (getline(file, line))
    {
		std::string::iterator end_pos = std::remove(line.begin(), line.end(), ' ');
		line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());

		//std::cout << line << std::endl;
        std::vector<std::string> vec;
        boost::algorithm::split(vec, line, boost::is_any_of(delimeter));
		
        dataList.push_back(vec);
    }
    // Close the File
    file.close();
    return dataList;
}
