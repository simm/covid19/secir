#include "settings.h"

//---------------------------------------------
//---------------------------------------------

settings::settings(std::string _fileAddress, std::string _fileName)
{
    fileNameAddress.append(_fileAddress);
    fileNameAddress.append(_fileName);
    std::ifstream file(fileNameAddress);
    std::string line;
    while(std::getline(file, line)){
        if (!line.empty()){
            boost::algorithm::trim(line);                       // Removing empty spaces in front and at the end of line
            std::replace(line.begin(), line.end(), '\t', ' ');  // Replacing tabs with spaces
            while (line.find( "  " ) != line.npos)              // Removing multiple spaces
                boost::replace_all( line, "  ", " ");

            std::string phrase;
            std::vector<std::string> row;
            std::stringstream ss(line);

            while(std::getline(ss, phrase, ' ')) {  // Split with space
                row.push_back(std::move(phrase));
            }
            lines.push_back(row);            
        }
    }
    lineSize = lines.size();
    loadParametersTag();
}

//---------------------------------------------
//---------------------------------------------

void settings::loadParametersTag(){
    double value_temp;
    int parameter_counter = 0;
    for (int i=0; i < lineSize; i++){
        if ((lines[i][0] == "_PARAMETER_VAR") || (lines[i][0]=="_PARAMETER_FIXED")){ // Parameter declaration
            paramTags.push_back(lines[i][1]);
            paramMax.push_back(0.0);
            paramMin.push_back(0.0);
            paramMaxFlex.push_back(false);
            paramMinFlex.push_back(false);
        }
    }
    for (int i=0; i < lineSize; i++){
        if (boost::iequals(lines[i][0], "_PARAMETER_FIXED")){
            value_temp = std::stod(lines[i][2]);
            paramMax[parameter_counter] = value_temp;
            paramMin[parameter_counter] = value_temp;
            parameter_counter++;
        }

        if (boost::iequals(lines[i][0], "_MAX"))
            paramMax[parameter_counter] = std::stod(lines[i][1]);
        if (boost::iequals(lines[i][0], "_MIN"))
            paramMin[parameter_counter] = std::stod(lines[i][1]);
        if (boost::iequals(lines[i][0], "_maxFlex"))
            if (boost::iequals(lines[i][1], "true"))
                paramMaxFlex[parameter_counter] = true;
        if (boost::iequals(lines[i][0], "_minFlex"))
            if (boost::iequals(lines[i][1], "true"))
                paramMinFlex[parameter_counter] = true;
        if (lines[i][0]=="}")
            parameter_counter++;
    }
}

//---------------------------------------------
//---------------------------------------------

void settings::processCmd(std::vector<std::string> cmdArg){
    for (int i = 0; i < cmdArg.size(); i++){ // Fixing a parameter to a value (from those given in paramTags),
                                             // if it is given in command line
        for (int j = 0; j < paramTags.size(); j++){
            if (cmdArg[i] == paramTags[j]){
                std::cout << cmdArg[i] << std::endl;
                paramMax[j] = std::stod(cmdArg[i+1]);
                paramMin[j] = std::stod(cmdArg[i+1]);
                paramMaxFlex[j] = false;
                paramMinFlex[j] = false;
            }
        }
    }
}

//---------------------------------------------
//---------------------------------------------
void settings::print_parameters(){
    std::cout << "Name of parameters: " << std::endl;
    if (paramTags.size() > 0)
        for (int i=0; i < paramTags.size(); i++)
            std::cout << paramTags[i] << "\t" << paramMax[i] << "\t" << paramMin[i] << "\t" << paramMaxFlex[i] << "\t" << paramMinFlex[i] << std::endl;
}
