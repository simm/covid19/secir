#ifndef GENERAL_FUNCTIONS_H
#define GENERAL_FUNCTIONS_H
#include <string>
#ifdef __linux__
    //linux code goes here
#elif _WIN32
    #include <direct.h>
#else

#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>

bool makeDir(std::string sessionAddress);

void quickSort(double *arr, int * order, int left, int right);

//char* current_dir();
std::string current_dir();

bool fileExists (const std::string& name);

#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <boost/algorithm/string.hpp>

/*
 * A class to read data from a csv file.
 */
class CSVReader{
    std::string fileName;
    std::string delimeter;
public:
    CSVReader(std::string filename, std::string delm = ",") :
            fileName(filename), delimeter(delm)
    { }

    // Function to fetch data from a CSV File
    std::vector<std::vector<std::string> > getData();
};

#endif // GENERAL_FUNCTIONS_H