#ifndef MODELS_H
#define MODELS_H

#include <math.h>
#include <string>
#include <vector>
#include <sstream> // std::ostringstream
#include <fstream>
#include <chrono>
using namespace std::chrono;
using namespace std;
#include "general_functions.h"
//------------------------------------------------------------------------------------------------------
extern std::vector<std::string> cmdArg; // Command-line arguments
//------------------------------------------------------------------------------------------------------
#include <iostream>
#include <boost/algorithm/string.hpp> // For comparing strings
#include <boost/filesystem.hpp>
#include <iterator>
#include <random>
#include <omp.h>
using namespace boost::filesystem;
#include <boost/numeric/odeint.hpp>
typedef std::vector< double > state_type;
//------------------------------------------------------------------------------------------------------
//using namespace std;
using namespace boost::numeric::odeint;
using namespace boost;
//------------------------------------------------------------------------------------------------------
enum Vars {S, E, Cr, Ci, Ih, Ir, Hu, Hr, Ud, Ur, D, Rz, Ix, Rx, Iprime, Hs, numVars};
enum parameters {
	R2_e, R3_e, R4_e, R5_e, R6_e, R7_e, R8_e, R9_e, R10_e, R4prime_e, R6prime_e, tau_e,
	alpha_e, beta_e, rho_e, theta_e, delta_e, mu_e, 
	mu_ds_a1_e, mu_ds_a2_e, mu_ds_t1_e, mu_ds_t2_e, mu_ds_C1_e, mu_ds_C2_e, mu_ds_Tmax_e, mu_ds_Ffinal_e, mu_ds_Fmin_e, mu_ds_starting_e, 
	cfr_h_e, cfr_l_e, cfr_k_e, cfr_t0_e, numParams
};
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
class iParameter{
public:
    iParameter(){}
    ~iParameter(){}
    double N0;
    double alpha, beta, chi, mu, delta, theta, rho;
    double R1, R2, R3, R4, R5, R6, R7, R8, R9, R10;
    // Additional parameters in the revised model
    double tau, R4prime, R6prime;
    double CFR_H, CFR_L, CFR_k, CFR_t0;
    // Parameters associated with time-varying mu
    double mu_ds_a1, mu_ds_a2, mu_ds_t1 = 0, mu_ds_t2 = 0, mu_ds_C1, mu_ds_C2, mu_ds_Tmax, mu_ds_Fmin, mu_ds_Ffinal, mu_ds_starting;

    double cost = 0.0;

    bool	enable_time_varying_mu		= false;
    bool	enable_time_varying_rho		= false;
    bool	enable_time_varying_theta	= false;
    bool	enable_time_varying_delta	= false;

    void print_param();
    void process_cmdArg();
};
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
class parameter_pool{
public:
    parameter_pool() {}
    ~parameter_pool() {}
    std::vector<iParameter> params;
    void load_pool_from_file(std::string filename);
    void generate_pool_random(int);
    void save_pool_random(std::string filename);
    void generate_pool_random_sensitivity();
    void process_cmdArg();
    void print_pool(){
        for (int i=0; i<params.size(); i++){
            params[i].print_param();
            std::cout << std::endl;
        }
    }	
};
//-------------------------------------------------
//-------------------------------------------------
class MODEL_revised {
public:
	iParameter p;
	double rho, theta, delta, Nt;
	double cfr_factor = 0, cfr_current, cfr_to_be;
	double mu_t, Ifinal, Ibase, sig1, sig2, mu_t_out;
    //-------------------------------------------------
	double CFR_factor(double t) {
		if (p.enable_time_varying_rho) {
			cfr_current = p.rho * p.theta * p.delta;
			cfr_to_be = p.CFR_H - (p.CFR_H - p.CFR_L) * (1.0 / (1.0 + exp(-p.CFR_k * (t - p.CFR_t0))));			
            cfr_factor = cfr_to_be / cfr_current;			
			rho = cfr_factor * p.rho;
			theta = p.theta;
			delta = p.delta;
		}
		else {
			rho   = p.rho;
			theta = p.theta;
			delta = p.delta;
		}
		return cfr_factor;
	}
    //-------------------------------------------------
	double mu_time_varying(double t) {
		if (p.enable_time_varying_mu) {
			Ifinal = p.mu_ds_Ffinal - p.mu_ds_Fmin;
			sig1 = 1.0 / (1.0 + exp(-p.mu_ds_a1 * (t - p.mu_ds_t1)));
			sig2 = 1.0 / (1.0 + exp(-p.mu_ds_a2 * (t - p.mu_ds_t2)));
			Ibase = sig1 * sig2;
			if (t > p.mu_ds_Tmax)	mu_t = p.mu_ds_Fmin + p.mu_ds_C2 * Ibase + Ifinal;
			else					mu_t = p.mu_ds_Fmin + p.mu_ds_C1 * Ibase;

			mu_t *= (p.mu / p.mu_ds_starting);
		}
		else mu_t = p.mu;
		return mu_t;
	}
    //-------------------------------------------------
	double rho_time_varying(double t) {
		double factor = CFR_factor(t);
		if (p.enable_time_varying_rho) return (p.rho * factor);
		else return (p.rho);
	}
    //-------------------------------------------------
	MODEL_revised() {}
    //-------------------------------------------------
	void operator() (const state_type& x, state_type& dxdt, double t) {		
		CFR_factor(t);
		mu_time_varying(t);
        Nt = p.N0 - x[D];        
		//--------------------------------------------------------------------------
		dxdt[S] = -p.R1 * (1.0 / Nt) * (x[Cr] + x[Ci] + p.chi * x[Ix] + p.beta * x[Ir] + p.beta * x[Ih] + x[Iprime]) * x[S];
		//--------------------------------------------------------------------------
		dxdt[E] =  p.R1 * (1.0 / Nt) * (x[Cr] + x[Ci] + p.chi * x[Ix] + p.beta * x[Ir] + p.beta * x[Ih] + x[Iprime]) * x[S] - p.R2 * x[E];
		//--------------------------------------------------------------------------
		dxdt[Ci] = (1.0 - p.alpha) * p.R2 * x[E] - p.R3 * x[Ci];
		dxdt[Cr] = p.alpha * p.R2 * x[E] - p.R9 * x[Cr];
		//--------------------------------------------------------------------------
		dxdt[Iprime] = mu_t * p.R3 * x[Ci] - p.tau * x[Iprime];
		//--------------------------------------------------------------------------
		dxdt[Ix] = (1.0 - mu_t) * p.R3 * x[Ci] - p.R4 * x[Ix];
		//--------------------------------------------------------------------------
		dxdt[Ih] = rho * p.tau * x[Iprime] - p.R6prime * x[Ih];
		dxdt[Ir] = (1.0 - rho) * p.tau * x[Iprime] - p.R4prime * x[Ir];
		//--------------------------------------------------------------------------
		dxdt[Hu] = theta * p.R6prime * x[Ih] - p.R7 * x[Hu];
		dxdt[Hr] = (1.0 - theta) * p.R6prime * x[Ih] - p.R5 * x[Hr];
		dxdt[Hs] = p.R8 * x[Ur] - p.R5 * x[Hs];
		//--------------------------------------------------------------------------
		dxdt[Ud] = delta * p.R7 * x[Hu] - p.R10 * x[Ud];
		dxdt[Ur] = (1.0 - delta) * p.R7 * x[Hu] - p.R8 * x[Ur];
		//--------------------------------------------------------------------------
        dxdt[Rz] = p.R4prime * x[Ir] + p.R5 * x[Hr] + p.R5 * x[Hs];
		dxdt[Rx] = p.R9 * x[Cr] + p.R4 * x[Ix];
		//--------------------------------------------------------------------------
		dxdt[D]  = p.R10 * x[Ud];
		//--------------------------------------------------------------------------
	}
};
//-------------------------------------------------
//-------------------------------------------------
class iData{
public:
    iData() {}
    ~iData(){}
    int datapointsNum;
    std::vector<std::string> date;
    std::vector<double>      time;
    std::vector<double>      day_of_year; // Starting from January 1st 2020
    std::vector<double>      infected_cumulative;
    std::vector<double>      infected_newCases;
    std::vector<double>      dead_cumulative;
    std::vector<double>      dead_newCases;
    double                   N0;
    void get_datapointsNum(){
        datapointsNum = date.size();
    }
    void print_data(){
        get_datapointsNum();
		for (int i = 0; i < datapointsNum; i++) {
			std::cout << "date: " << date[i] << " cumulative: " << infected_cumulative[i] << " N0 " << N0 << std::endl;
		}
    }
    void load_data_from_file(std::string);
    std::string dataFileNameAddress;
};
//-------------------------------------------------
//-------------------------------------------------
class windowEval{
public:
    windowEval(iData, iParameter);
    ~windowEval();

    MODEL_revised model;
    iData data;
    iParameter param;
    iParameter param_org;
    int iter = 0;
    std::string tag = "single_run";

    double  RSS = 0.0;
    state_type initialCondition;
    state_type x;
    double     initialTime;

    int     timeShift  = 1;
    int     windowSize = 10;
    int     startingDate_idx;
    int     endingDate_idx;
    int     datapointsNum;
    int     windowTotalNum;
    int     windowID;

    double  evaluation(double);
    void    print_param();
    void    load_param(iParameter);
    void    load_data(iData);
    void    reset();

    double  fit_simplex(int, double);
    void    fit_run();

    double  calculate_Rt();
    void    write_to_file();
    void    set_folder_address();

    void    process_cmdArg();

    double  SSE_infected_cumulative;
    double  SSE_dead_cumulative;

    bool    report_to_file = false;
    std::string sessionName;
    std::string scenarioName;
    std::string cityName;
    //-----------------------------------------
    // Observers to be reported in the output file, write below
    std::vector<double> r1_optimum;
    std::vector<double> Rt;	
    std::vector<std::string> Rt_date;
    std::vector<double> fitting_cost;
    std::vector<double> fitting_cost_cumulativeDead;
    std::vector<double> fitting_cost_cumulativeInfected;
    double fitting_cost_cumulativeDead_total;
    double fitting_cost_cumulativeInfected_total;
    //-----------------------------------------
private:                
    std::vector<double>      sim_infected_cumulative;
    std::vector<double>      sim_infected_newCases;
    std::vector<double>      sim_dead_cumulative;
    std::vector<double>      sim_dead_newCases;
    std::vector<double>      sim_recovered;

    std::vector<double>      mu_t;
    std::vector<double>      rho_t;
    std::vector<double>      delta_t;
    std::vector<double>      theta_t;

    std::vector<double>      error_infected_cumulative;
    std::vector<double>      error_dead_cumulative;

    std::vector<std::vector<double>>	S_out, E_out, Cr_out, Ci_out, Ih_out, Ir_out,
                                        Hu_out, Hr_out, Ur_out, Ud_out, D_out, Ix_out,
                                        Rx_out, Rz_out, Iprime_out, Hs_out,
                                        mu_out, rho_out, theta_out, delta_out,
                                        time_out, date_out;

    // Output file variables:
    std::ofstream outFile;
    std::string sessionAddress;
    std::string dataFileNameAddress;
    std::string outFileNameAddress_parameters;
    std::string outFileNameAddress_Rt;
    std::string outFileNameAddress_R1;
    std::string outFileNameAddress_fitting_cost;
    std::string outFileNameAddress_date;
    std::string outFileNameAddress_time;

    std::string outFileNameAddress_S;
    std::string outFileNameAddress_E;
    std::string outFileNameAddress_Cr;
    std::string outFileNameAddress_Ci;
    std::string outFileNameAddress_Ih;
    std::string outFileNameAddress_Ir;
    std::string outFileNameAddress_Hu;
    std::string outFileNameAddress_Hr;
    std::string outFileNameAddress_Ur;
    std::string outFileNameAddress_Ud;
    std::string outFileNameAddress_D;
    std::string outFileNameAddress_Ix;
    std::string outFileNameAddress_Rx;
    std::string outFileNameAddress_Rz;
    std::string outFileNameAddress_Hs;
    std::string outFileNameAddress_Iprime;
    std::string outFileNameAddress_mu;
    std::string outFileNameAddress_rho;
    std::string outFileNameAddress_theta;
    std::string outFileNameAddress_delta;
};
//--------------------------------------------------
//--------------------------------------------------
#endif // MODELS_H