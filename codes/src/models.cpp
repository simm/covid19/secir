#include "models.h"

const char* Vars_symbol[numVars] = { "S", "E", "Cr", "Ci", "Ih", "Ir", "Hu", "Hr", "Ud", "Ur", "D", "Rz", "Ix", "Rx", "Iprime", "Hs" };

std::default_random_engine generator(std::random_device{}());
std::uniform_real_distribution<double> distribution(0.0,1.0);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void parameter_pool::load_pool_from_file(std::string filename){
    // Creating an object of CSVWriter
    CSVReader reader(filename);
    // Get the data from CSV File
    std::vector<std::vector<std::string> > dataList = reader.getData();
    for (int i = 1; i < dataList.size(); i++){
        iParameter ipar;
        for (int j = 0; j < dataList[i].size(); j++){
            //std::cout<< dataList[i][j] << "\t";
			if		(boost::iequals(dataList[0][j], "R2"))		ipar.R2 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R3"))		ipar.R3 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R4"))		ipar.R4 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R5"))		ipar.R5 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R6"))		ipar.R6 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R7"))		ipar.R7 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R8"))		ipar.R8 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R9"))		ipar.R9 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R10"))		ipar.R10 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "alpha"))	ipar.alpha = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "beta"))	ipar.beta = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "theta"))	ipar.theta = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "delta"))	ipar.delta = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "rho"))		ipar.rho = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu"))		ipar.mu = std::stod(dataList[i][j]);

            else if (boost::iequals(dataList[0][j], "chi"))		ipar.chi   = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "tau"))		ipar.tau = std::stod(dataList[i][j]);

			else if (boost::iequals(dataList[0][j], "R4prime"))			ipar.R4prime = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "R6prime"))			ipar.R6prime = std::stod(dataList[i][j]);

			else if (boost::iequals(dataList[0][j], "mu_ds_starting"))	ipar.mu_ds_starting = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_a1"))		ipar.mu_ds_a1 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_a2"))		ipar.mu_ds_a2 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_t1"))		ipar.mu_ds_t1 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_t2"))		ipar.mu_ds_t2 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_Tmax"))		ipar.mu_ds_Tmax = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_C1"))		ipar.mu_ds_C1 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_C2"))		ipar.mu_ds_C2 = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_Fmin"))		ipar.mu_ds_Fmin = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "mu_ds_Ffinal"))	ipar.mu_ds_Ffinal = std::stod(dataList[i][j]);

			else if (boost::iequals(dataList[0][j], "CFR_H"))   ipar.CFR_H = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "CFR_L"))   ipar.CFR_L = std::stod(dataList[i][j]);
			else if (boost::iequals(dataList[0][j], "CFR_k"))   ipar.CFR_k = std::stod(dataList[i][j]);
			//----------------------------------------------------------------------------------------------
			// Last column always with icontains instead of iequals. Otherwise, the data cannot be loaded.
			//----------------------------------------------------------------------------------------------
			else if (boost::icontains(dataList[0][j], "CFR_t0"))  ipar.CFR_t0= std::stod(dataList[i][j]); 
        }
        params.push_back(ipar);
    }
	process_cmdArg();
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void parameter_pool::process_cmdArg() {
	for (int i = 0; i < params.size(); i++) {
		params[i].process_cmdArg();
	}
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void iParameter::process_cmdArg() {
	for (int i = 0; i < cmdArg.size(); i++) {
		if (cmdArg[i] == "R1")			R1		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R2")			R2		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R3")			R3		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R4")			R4		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R4prime")		R4prime = stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R5")			R5		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R6")			R6		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R6prime")		R6prime = stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R7")			R7		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R8")			R8		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R9")			R9		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "R10")			R10		= stod(cmdArg[i + 1]);

		if (cmdArg[i] == "tau")			tau		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "alpha")		alpha	= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "beta")		beta	= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "chi")			chi		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "mu")			mu		= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "delta")		delta	= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "theta")		theta	= stod(cmdArg[i + 1]);
		if (cmdArg[i] == "rho")			rho		= stod(cmdArg[i + 1]);

		if (cmdArg[i] == "enable_time_varying_rho")		enable_time_varying_rho		= true;
		if (cmdArg[i] == "enable_time_varying_mu")		enable_time_varying_mu		= true;
		if (cmdArg[i] == "enable_time_varying_theta")	enable_time_varying_theta	= true;
		if (cmdArg[i] == "enable_time_varying_delta")	enable_time_varying_delta	= true;

        if (cmdArg[i] == "CFR_H")   CFR_H   = stod(cmdArg[i + 1]);
        if (cmdArg[i] == "CFR_L")   CFR_L   = stod(cmdArg[i + 1]);
        if (cmdArg[i] == "CFR_k")   CFR_k   = stod(cmdArg[i + 1]);
        if (cmdArg[i] == "CFR_t0")  CFR_t0  = stod(cmdArg[i + 1]);
	}
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
windowEval::windowEval(iData dataset, iParameter _param): initialCondition(numVars), x(numVars), model() {
    startingDate_idx = 0;
    endingDate_idx   = startingDate_idx + windowSize - 1;
    load_data(dataset);
    load_param(_param);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
windowEval::~windowEval() {
	std::cout << "windowEval destructed." << std::endl;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void windowEval::load_param(iParameter _param){
    param = _param;
    param.R1 = 0.0;
    param_org = _param;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void windowEval::load_data(iData dataset){
    data = dataset;
    data.get_datapointsNum();

    windowTotalNum = data.date.size() - windowSize + 1;

    r1_optimum.reserve(windowTotalNum);
    Rt.reserve(windowTotalNum);	
	fitting_cost.reserve(windowTotalNum);
	fitting_cost_cumulativeDead.reserve(windowTotalNum);
	fitting_cost_cumulativeInfected.reserve(windowTotalNum);

	sim_infected_cumulative.reserve(windowSize + 1);
	sim_infected_newCases.reserve(windowSize + 1);
	sim_dead_cumulative.reserve(windowSize + 1);
	sim_dead_newCases.reserve(windowSize + 1);
	sim_recovered.reserve(windowSize + 1);

	error_infected_cumulative.reserve(windowSize);
	error_dead_cumulative.reserve(windowSize);
	mu_t.reserve(windowSize);
	rho_t.reserve(windowSize);
	delta_t.reserve(windowSize);
	theta_t.reserve(windowSize);
	
	// Initializing vectors	
	for (int i = 0; i < windowTotalNum; i++){
		Rt_date.push_back(data.date[windowSize - 1 + i]);
		Rt.push_back(0);
		r1_optimum.push_back(0);
		fitting_cost.push_back(0);
		fitting_cost_cumulativeDead.push_back(0);
		fitting_cost_cumulativeInfected.push_back(0);
	}

	for (int i = 0; i < (windowSize + 1); i++) {
		sim_infected_cumulative.push_back(0);
		sim_infected_newCases.push_back(0);
		sim_dead_cumulative.push_back(0);
                sim_dead_newCases.push_back(0);
	}

	for (int i = 0; i < windowSize; i++) {
		error_infected_cumulative.push_back(0);
		error_dead_cumulative.push_back(0);
                mu_t.push_back(0);
                rho_t.push_back(0);
                delta_t.push_back(0);
                theta_t.push_back(0);
	}

    auto it = data.time.insert(data.time.begin(), 1, -5.9); // time -5.9 days is added to the beginning of time.
															// Influxes are assumed at 4 consequtive days starting from 
															// incubation_period + clinical delay, i.e. -(5.2+3.7) = -8.9, -7.9, -6.9 and -5.9 (4 consequetive days).
															// That is why the rest of the simulation starts from -5.9.
	data.time[0] += data.time[1];	// In the revised model, we used the day_of_year as the basis of time.
									// Therefore, the starting time of the simulation is corrected here based on the day of the year.
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void windowEval::print_param(){
    param.print_param();
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void windowEval::reset(){
	// In case that one object windowEval is used for multiple simulations, reset() needs to be called AFTER changing param_org
    param_org.N0 = data.N0;
    param        = param_org;

    startingDate_idx= 0;
    endingDate_idx  = startingDate_idx + windowSize - 1;
    windowID		= 0;
    model.p			= param_org;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
double windowEval::calculate_Rt(){
	double No = data.N0 - initialCondition[D];
	double So = initialCondition[S];
	double mu_mean = 0, rho_mean = 0, delta_mean = 0, theta_mean = 0, Nt_mean = 0;
	for (int i = 0; i < windowSize; i++) {
		mu_mean    += mu_t   [i] / windowSize;
        rho_mean   += rho_t  [i] / windowSize;
		Nt_mean    += (param_org.N0 - sim_dead_cumulative[i + 1]) / windowSize;
	}
	return (param.R1 * (So / Nt_mean) *
			(			
			param.alpha / param.R9 +
			(1.0 - param.alpha) / param.R3 +
			(1.0 - param.alpha) * (1.0 - mu_mean) / param.R4 +
			mu_mean * (1.0 - param.alpha) / param.tau +
			param.beta * mu_mean * (1.0 - param.alpha) * (1.0 - rho_mean) / param.R4prime +
			param.beta * mu_mean * rho_mean * (1.0 - param.alpha) / param.R6prime
			)
		);
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
double windowEval::evaluation(double r1) {
	//------------------------------------------------------------------------------------------------------------------------------------
	typedef runge_kutta_cash_karp54< state_type > error_stepper_type;               // define_adapt_stepper
	typedef controlled_runge_kutta< error_stepper_type > controlled_stepper_type;   // integrate_adapt
	double abs_err = 1.0e-8, rel_err = 1.0e-8, a_x = 1.0, a_dxdt = 1.0;          // integrate_adapt_full
	controlled_stepper_type controlled_stepper(default_error_checker< double, range_algebra, default_operations >(abs_err, rel_err, a_x, a_dxdt));
	//------------------------------------------------------------------------------------------------------------------------------------    
	model.p.R1 = r1;
	model.p.chi = 1.0;
	//model.p.print_param();
	
	// Implementing the initial condition with influxes.
	if (windowID == 0) {
		double incubation_period = 5.2;
		double clinical_delay    = 3.7;
		double starting_time     = data.time[1]-incubation_period-clinical_delay;
		double detection_ratio	 = (1.0 - param.alpha) * param.mu;

		double influx_0 = 0.0, influx_1 = 0.0, influx_2 = 0.0, influx_3 = 0.0;

		influx_0 = data.infected_newCases[0] / detection_ratio;
		influx_1 = data.infected_newCases[1] / detection_ratio;
		influx_2 = data.infected_newCases[2] / detection_ratio;
		influx_3 = data.infected_newCases[3] / detection_ratio;

		x[S]  = param_org.N0;
		x[E]  = influx_0;
		x[Cr] = 0; x[Ci] = 0; x[Ih] = 0; x[Ir] = 0;
		x[Hu] = 0; x[Hr] = 0; x[Ud] = 0; x[Ur] = 0;
		x[D]  = 0; x[Rz] = 0; x[Ix] = 0; x[Rx] = 0; x[Iprime] = 0; x[Hs] = 0;
		integrate_adaptive(controlled_stepper, model, x, starting_time + 0.0, starting_time + 1.0, 1e-8);	x[E] = x[E] + influx_1;
		integrate_adaptive(controlled_stepper, model, x, starting_time + 1.0, starting_time + 2.0, 1e-8);	x[E] = x[E] + influx_2;
		integrate_adaptive(controlled_stepper, model, x, starting_time + 2.0, starting_time + 3.0, 1e-8);	x[E] = x[E] + influx_3;		
		initialCondition = x;
	}
	x = initialCondition;
	
	// Initial value for each time-window
	sim_infected_cumulative[0] = x[Ih] + x[Ir] + x[Hu] + x[Hr] + x[Ur] + x[Ud] + x[D] + x[Rz] + x[Hs];
	sim_dead_cumulative    [0] = x[D];
	sim_recovered          [0] = x[Rz];
	sim_infected_newCases  [0] = 0.0;
	sim_dead_newCases      [0] = 0.0;
	
	if (report_to_file) {
		// Initial value for each time-window
        S_out[windowID][0]  = x[S];	E_out[windowID][0]  = x[E];
		Cr_out[windowID][0] = x[Cr];	Ci_out[windowID][0] = x[Ci];
		Ih_out[windowID][0] = x[Ih];	Ir_out[windowID][0] = x[Ir];
		Hu_out[windowID][0] = x[Hu];	Hr_out[windowID][0] = x[Hr];
		Ur_out[windowID][0] = x[Ur];	Ud_out[windowID][0] = x[Ud];
        D_out[windowID][0]  = x[D];		Ix_out[windowID][0] = x[Ix];
		Rx_out[windowID][0] = x[Rx];	Rz_out[windowID][0] = x[Rz];
		Hs_out[windowID][0] = x[Hs];	Iprime_out[windowID][0] = x[Iprime];

		mu_out    [windowID][0] = model.mu_time_varying    (data.time[startingDate_idx]);
        rho_out   [windowID][0] = model.rho_time_varying   (data.time[startingDate_idx]);
	}
	
	int steps;
	for (int i = 0; i < windowSize; i++) {
		//std::cout << "Integration from time " << data.time[startingDate_idx + i] << " to " << data.time[startingDate_idx + i + 1] << std::endl;
		steps = integrate_adaptive(controlled_stepper, model, x, data.time[startingDate_idx + i], data.time[startingDate_idx + i + 1], 1e-8);
		mu_t[i]    = model.mu_time_varying    (data.time[startingDate_idx + i + 1]);
		rho_t[i]   = model.rho_time_varying   (data.time[startingDate_idx + i + 1]);
		
		sim_infected_cumulative[i + 1] = x[Ih] + x[Ir] + x[Hu] + x[Hr] + x[Ur] + x[Ud] + x[D] + x[Rz] + x[Hs];
		sim_dead_cumulative[i + 1] = x[D];
		sim_recovered[i + 1] = x[Rz];
		
		if (report_to_file) {
			S_out [windowID][i + 1] = x[S];		E_out [windowID][i + 1]  = x[E];
			Cr_out[windowID][i + 1] = x[Cr];	Ci_out[windowID][i + 1] = x[Ci];
			Ih_out[windowID][i + 1] = x[Ih];	Ir_out[windowID][i + 1] = x[Ir];
			Hu_out[windowID][i + 1] = x[Hu];	Hr_out[windowID][i + 1] = x[Hr];
			Ur_out[windowID][i + 1] = x[Ur];	Ud_out[windowID][i + 1] = x[Ud];
			D_out [windowID][i + 1] = x[D];		Ix_out[windowID][i + 1] = x[Ix];
			Rx_out[windowID][i + 1] = x[Rx];	Rz_out[windowID][i + 1] = x[Rz];
			Hs_out[windowID][i + 1] = x[Hs];	Iprime_out[windowID][i + 1] = x[Iprime];

			mu_out    [windowID][i + 1] = model.mu_time_varying    (data.time[startingDate_idx + i + 1]);
			rho_out   [windowID][i + 1] = model.rho_time_varying   (data.time[startingDate_idx + i + 1]);
		}
	}

	for (int i = 1; i < (windowSize + 1); i++) {
		sim_infected_newCases[i] = sim_infected_cumulative[i] - sim_infected_cumulative[i - 1];
		sim_dead_newCases    [i] = sim_dead_cumulative    [i] - sim_dead_cumulative    [i - 1];
	}

	// Calculate error
	SSE_infected_cumulative = 0.0;
	SSE_dead_cumulative     = 0.0;
	for (int i = 0; i < windowSize; i++) {
		error_infected_cumulative[i] = (data.infected_cumulative [startingDate_idx + i] - sim_infected_cumulative[i + 1]) / data.infected_cumulative[endingDate_idx];
		error_dead_cumulative    [i] = (data.dead_cumulative     [startingDate_idx + i] - sim_dead_cumulative    [i + 1]);
		SSE_infected_cumulative += pow(error_infected_cumulative[i], 2) / windowSize;
	}
	SSE_dead_cumulative = pow(error_dead_cumulative.back() / data.dead_cumulative.back(), 2);	
	return SSE_infected_cumulative;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void windowEval::fit_run(){
    reset();

	if (report_to_file) {
		set_folder_address();
	}

    //------------------------------------------------------------------------------------------------------------------------------------
    typedef runge_kutta_cash_karp54< state_type > error_stepper_type;               // define_adapt_stepper
    typedef controlled_runge_kutta< error_stepper_type > controlled_stepper_type;   // integrate_adapt
    double abs_err = 1.0e-8 , rel_err = 1.0e-8 , a_x = 1.0 , a_dxdt = 1.0;          // integrate_adapt_full
    controlled_stepper_type controlled_stepper(default_error_checker< double , range_algebra , default_operations >( abs_err , rel_err , a_x , a_dxdt ) );
    //------------------------------------------------------------------------------------------------------------------------------------

    for (int i=0; i<windowTotalNum; i++){
        r1_optimum[windowID] = fit_simplex(windowID, 0.45); // Optimization	

        param.R1     = r1_optimum[windowID];
        Rt[windowID] = calculate_Rt();        
		
		fitting_cost					[windowID] = evaluation(r1_optimum[windowID]);
		fitting_cost_cumulativeDead     [windowID] = SSE_dead_cumulative;
		fitting_cost_cumulativeInfected [windowID] = SSE_infected_cumulative;

		//----------------------------------------------------------------------------------------------------------------------
		// Initial condition for the next step
		//----------------------------------------------------------------------------------------------------------------------
		x = initialCondition;
        model.p.R1 = r1_optimum[windowID];
        integrate_adaptive(controlled_stepper, model, x, data.time[startingDate_idx],
														 data.time[startingDate_idx + 1],  1e-8);
        initialCondition = x;
		//----------------------------------------------------------------------------------------------------------------------
		// Shifting window
        startingDate_idx += timeShift;
        endingDate_idx   += timeShift;
        windowID += 1;
    }

	fitting_cost_cumulativeDead_total     = 0.0;
	fitting_cost_cumulativeInfected_total = 0.0;
	for (int i = 0; i < windowTotalNum; i++) {
		fitting_cost_cumulativeDead_total     += fitting_cost_cumulativeDead     [i] / windowTotalNum;
		fitting_cost_cumulativeInfected_total += fitting_cost_cumulativeInfected [i] / windowTotalNum;
	}
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void iParameter::print_param() {
	std::cout << "--------------------\n";
	std::cout << "R1 " << R1;
	std::cout << "| R2 " << R2;
	std::cout << "| R3 " << R3;
	std::cout << "| R4prim " << R4prime;
	std::cout << "| R5 " << R5;
	std::cout << "| R6p " << R6prime;
	std::cout << "| R7 " << R7;
	std::cout << "| R8 " << R8;
	std::cout << "| R9 " << R9;
	std::cout << "| R10 " << R10;
    std::cout << "| tau " << tau;
	std::cout << std::endl;
	std::cout << "| alpha " << alpha;
	std::cout << "| beta " << beta;
	std::cout << "| delta " << delta;
	std::cout << "| theta " << theta;
	std::cout << "| rho " << rho;
	std::cout << "| mu " << mu;
	std::cout << "| chi " << chi;
	std::cout << "| N0 " << N0;
	std::cout << std::endl;
	std::cout << "| mu_ds_a1 " << mu_ds_a1;
	std::cout << "| mu_ds_a2 " << mu_ds_a2;
	std::cout << "| mu_ds_t1 " << mu_ds_t1;
	std::cout << "| mu_ds_t2 " << mu_ds_t2;
	std::cout << "| mu_ds_C1 " << mu_ds_C1;
	std::cout << "| mu_ds_C2 " << mu_ds_C2;
	std::cout << "| mu_ds_Tmax " << mu_ds_Tmax;
	std::cout << "| mu_ds_Ffinal " << mu_ds_Ffinal;
	std::cout << "| mu_ds_Fmin " << mu_ds_Fmin;
	std::cout << "| mu_ds_starting " << mu_ds_starting;
	std::cout << std::endl;
	
	std::cout << "| enable_time_varying_mu " << enable_time_varying_mu;
    std::cout << "| enable_time_varying_rho " << enable_time_varying_rho;
    std::cout << "| enable_time_varying_theta " << enable_time_varying_theta;
    std::cout << "| enable_time_varying_delta " << enable_time_varying_delta;
	std::cout << std::endl;

	std::cout << "| CFR_H " << CFR_H;
	std::cout << "| CFR_L " << CFR_L;
	std::cout << "| CFR_k " << CFR_k;
	std::cout << "| CFR_t0 " << CFR_t0;
	std::cout << std::endl;
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void iData::load_data_from_file(std::string _dataFileNameAddress) {
	dataFileNameAddress = _dataFileNameAddress;
	// Creating an object of CSVWriter
	CSVReader reader(dataFileNameAddress);
	// Get the data from CSV File
	std::vector<std::vector<std::string> > dataList = reader.getData();
	//std::cout << "datalist " << dataList.size() << std::endl;
	for (int i = 1; i < dataList.size(); i++) {
		for (int j = 0; j < dataList[0].size(); j++) {
			if		(boost::icontains(dataList[0][j], "date"))					date.push_back(dataList[i][j]);
			else if (boost::icontains(dataList[0][j], "day_of_year"))			time.push_back(std::stod(dataList[i][j])); //****************************************
			else if (boost::icontains(dataList[0][j], "infected_cumulative"))	infected_cumulative.push_back(std::stod(dataList[i][j]));
			else if (boost::icontains(dataList[0][j], "infected_newCases"))		infected_newCases.push_back(std::stod(dataList[i][j]));
			else if (boost::icontains(dataList[0][j], "dead_cumulative"))		dead_cumulative.push_back(std::stod(dataList[i][j]));
			else if (boost::icontains(dataList[0][j], "dead_newCases"))			dead_newCases.push_back(std::stod(dataList[i][j]));
			// Last column always with icontains instead of iequals. Otherwise, the data cannot be loaded.
			else if (boost::icontains(dataList[0][j], "population"))			N0 = (std::stod(dataList[i][j]));
		}
	}
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void windowEval::set_folder_address() {
	sessionAddress = current_dir() + "/results/" + sessionName + "/"; // Working address
	std::string mkDIR = current_dir() + "/results/" + sessionName + "/" + cityName + "/" + scenarioName;
	std::cout << "Make address:|"<< mkDIR << "|" <<std::endl;
	makeDir(current_dir() + "/results/" + sessionName + "/" + cityName);
	//makeDir(current_dir() + "/results/" + sessionName + "/" + cityName + "/" + scenarioName);

	outFileNameAddress_Rt			= current_dir() + "/results/" + sessionName + "/" + cityName +  "/" + cityName + "_Rt_" + tag + ".csv";
	outFileNameAddress_R1			= current_dir() + "/results/" + sessionName + "/" + cityName + "/" +  cityName + "_R1_" + tag + ".csv";
	outFileNameAddress_date			= current_dir() + "/results/" + sessionName + "/" + cityName + "/" +  cityName + "_date_" + tag + ".csv";
	outFileNameAddress_time			= current_dir() + "/results/" + sessionName + "/" + cityName + "/" +  cityName + "_time_" + tag + ".csv";
	outFileNameAddress_parameters	= current_dir() + "/results/" + sessionName + "/" + cityName + "/" +  cityName + "_parameters_" + tag + ".csv";;
	outFileNameAddress_fitting_cost	= current_dir() + "/results/" + sessionName + "/" + cityName + "/" +  cityName + "_fitting_cost_" + tag + ".csv";;

	outFileNameAddress_S		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_S_" + tag + ".csv";
	outFileNameAddress_E		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_E_" + tag + ".csv";
	outFileNameAddress_Cr		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Cr_" + tag + ".csv";
	outFileNameAddress_Ci		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Ci_" + tag + ".csv";
	outFileNameAddress_Ih		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Ih_" + tag + ".csv";
	outFileNameAddress_Ir		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Ir_" + tag + ".csv";
	outFileNameAddress_Hu		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Hu_" + tag + ".csv";
	outFileNameAddress_Hr		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Hr_" + tag + ".csv";
	outFileNameAddress_Ur		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Ur_" + tag + ".csv";
	outFileNameAddress_Ud		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Ud_" + tag + ".csv";
	outFileNameAddress_D		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_D_" + tag + ".csv";
	outFileNameAddress_Ix		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Ix_" + tag + ".csv";
	outFileNameAddress_Rx		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Rx_" + tag + ".csv";
	outFileNameAddress_Rz		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Rz_" + tag + ".csv";
	outFileNameAddress_Hs		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Hs_" + tag + ".csv";
	outFileNameAddress_Iprime	= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_Iprime_" + tag + ".csv";
	
	outFileNameAddress_mu		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_mu_" + tag + ".csv";
	outFileNameAddress_rho		= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_rho_" + tag + ".csv";
	outFileNameAddress_theta	= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_theta_" + tag + ".csv";
	outFileNameAddress_delta	= current_dir() + "/results/" + sessionName + "/" + cityName + "/" + cityName + "_delta_" + tag + ".csv";
	
	if (S_out.size() == 0){ // If it was not previously initialized
		S_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		E_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Cr_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Ci_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Ih_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Ir_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Hu_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Hr_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Ur_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Ud_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		D_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Ix_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Rx_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Rz_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Hs_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		Iprime_out	= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		mu_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		rho_out		= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		theta_out	= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		delta_out	= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
		time_out	= std::vector<std::vector<double>>(windowTotalNum, std::vector<double>(windowSize + 1, 0.0));
	}
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void windowEval::write_to_file() {
	outFile.open(outFileNameAddress_parameters.c_str(), std::ofstream::trunc);
	outFile << "R2,R3,R4,R4prime,R5,R6,R6prime,R7,R8,R9,R10,alpha,beta,rho,theta,delta,mu,tau,enable_time_varying_mu,enable_time_varying_rho,enable_time_varying_theta,enable_time_varying_delta,windowSize,CFR_H,CFR_L,CFR_k,CFR_t0" << std::endl;
	outFile	<< std::to_string(param.R2)			<< ","
			<< std::to_string(param.R3)			<< ","
			<< std::to_string(param.R4)			<< ","
			<< std::to_string(param.R4prime)	<< ","
			<< std::to_string(param.R5)			<< ","
			<< std::to_string(param.R6)			<< ","
			<< std::to_string(param.R6prime)	<< ","
			<< std::to_string(param.R7)			<< ","
			<< std::to_string(param.R8)			<< ","
			<< std::to_string(param.R9)			<< ","
			<< std::to_string(param.R10)		<< ","
			<< std::to_string(param.alpha)		<< ","
			<< std::to_string(param.beta)		<< ","
			<< std::to_string(param.rho)		<< ","
			<< std::to_string(param.theta)		<< ","
			<< std::to_string(param.delta)		<< ","
			<< std::to_string(param.mu)			<< ","
			<< std::to_string(param.tau)		<< ","
			<< std::to_string(param.enable_time_varying_mu)    << ","
			<< std::to_string(param.enable_time_varying_rho)   << ","
			<< std::to_string(param.enable_time_varying_theta) << ","
			<< std::to_string(param.enable_time_varying_delta) << ","
			<< std::to_string(windowSize)		<< ","
			<< std::to_string(param.CFR_H)		<< ","
			<< std::to_string(param.CFR_L)		<< ","
			<< std::to_string(param.CFR_k)		<< ","
			<< std::to_string(param.CFR_t0)		<< std::endl;
	outFile.close();

	outFile.open(outFileNameAddress_Rt.c_str(), std::ofstream::trunc);	for (int w = 0; w < windowTotalNum; w++) { outFile << std::to_string(Rt[w])			<< std::endl; }; outFile.close();
	outFile.open(outFileNameAddress_R1.c_str(), std::ofstream::trunc);	for (int w = 0; w < windowTotalNum; w++) { outFile << std::to_string(r1_optimum[w])	<< std::endl; }; outFile.close();
	outFile.open(outFileNameAddress_date.c_str(), std::ofstream::trunc);for (int w = 0; w < windowTotalNum; w++) { outFile << Rt_date[w]					<< std::endl; }; outFile.close();
	outFile.open(outFileNameAddress_fitting_cost.c_str(), std::ofstream::trunc); for (int w = 0; w < windowTotalNum; w++) { outFile << fitting_cost[w] << std::endl; }; outFile.close();
	
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_time.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(data.time[w + z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		} 	
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_S.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(S_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_E.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(E_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Cr.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Cr_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Ci.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Ci_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------	
	outFile.open(outFileNameAddress_Ih.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Ih_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Ir.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Ir_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Hu.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Hu_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Hr.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Hr_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Ur.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Ur_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Ud.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Ud_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_D.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(D_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Ix.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Ix_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Rx.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Rx_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Rz.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Rz_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Hs.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Hs_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_Iprime.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(Iprime_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_mu.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(mu_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_rho.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(rho_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_theta.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(theta_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
	//------------------------------------------------------------------
	outFile.open(outFileNameAddress_delta.c_str(), std::ofstream::trunc);
	for (int w = 0; w < windowTotalNum; w++) {
		for (int z = 0; z < (windowSize + 1); z++) {
			outFile << std::to_string(delta_out[w][z]);
			if (z < windowSize) outFile << ",";
			else outFile << std::endl;
		}
	}
	outFile.close();
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// This estimates optimal R1 in each window, using Nelder-Mead simplex algorithm
double windowEval::fit_simplex(int _windowID, double p1) {
	bool Report = false;
	double p2 = 2 * p1;

	double alpha = 1;
	double lambda = 2;
	double beta = 0.5;
	double delta = 0.5;
	double sigma = 0;
	double tolerance = 1e-10;
	int numEval = 0; int numEvalMax = 5000;

	double pBest, pWorst;
	double cBest, cWorst;
	double c1, c2;
	double centroid;

	double pReflect, cReflect;
	double pExpand, cExpand;
	double pContract, cContract;
	c1 = evaluation(p1);
	c2 = evaluation(p2);


	if (c1 > c2) { pWorst = p1; cWorst = c1; pBest = p2; cBest = c2; }
	else { pWorst = p2; cWorst = c2; pBest = p1; cBest = c1; }

	bool GoNext = false, continueSimplex = true;

	while (continueSimplex) {
		numEval += 1;
		GoNext = true;

		centroid = pBest;
		pReflect = (centroid + alpha * (centroid - pWorst));
		cReflect = evaluation(abs(pReflect));

		//--------- Replace
		if ((cReflect < cWorst) & (cReflect >= cBest)) {
			pWorst = pReflect;
			cWorst = cReflect;
			GoNext = false;
			if (Report) std::cout << "Replaced successfully." << std::endl;
		}
		//--------- Expansion
		if ((cReflect < cBest) & GoNext) {
			// Expansion: Perhaps the minimum is just a bit farther?
			//pExpand = abs(pReflect + lambda * (pReflect - centroid));
			pExpand = (centroid + lambda * (centroid - pWorst));
			cExpand = evaluation(abs(pExpand));
			if (Report) std::cout << "Expansion: " << pExpand << " " << cExpand << std::endl;
			if (cExpand < cReflect) {
				pWorst = pExpand;
				cWorst = cExpand;
				GoNext = false;
				if (Report) std::cout << "Expanded successfully." << std::endl;
			}
			else {
				pWorst = pReflect;
				cWorst = cReflect;
				GoNext = false;
				if (Report) std::cout << "Expanded failed, reflection successfully." << std::endl;
			}
		}
		//--------- Contraction outside
		if ((cReflect >= cWorst) & GoNext) {
			pContract = (centroid + beta * (pWorst - centroid));
			cContract = evaluation(abs(pContract));
			if (Report) std::cout << "Contraction 1: " << pContract << " " << cContract << std::endl;
			if (cContract < cWorst) {
				pWorst = pContract;
				cWorst = cContract;
				GoNext = false;
				if (Report) std::cout << "Contraction 1: Successful." << std::endl;
			}
		}
		//--------- Shrinking
		if (((cContract > cReflect) | (cContract > cWorst)) & GoNext) {
			pWorst = (pBest + delta * (pWorst - pBest));
			cWorst = evaluation(abs(pWorst));
			if (Report) std::cout << "Shrinking 1: " << " | " << pWorst << " " << cWorst << std::endl;
			if (Report) std::cout << "Shrinking successful." << std::endl;
		}

		p1 = pBest;  c1 = cBest;
		p2 = pWorst; c2 = cWorst;

		if (c1 > c2) { pWorst = p1; cWorst = c1; pBest = p2; cBest = c2; }
		else { pWorst = p2; cWorst = c2; pBest = p1; cBest = c1; }

		sigma = 2.0 * (cWorst - cBest) / (cWorst + cBest + tolerance);

		if (Report) std::cout << "sigma " << sigma << " tolerance " << tolerance << std::endl;
		if (sigma < tolerance) {
			continueSimplex = false;
			//std::cout << "Tolerance reached" << std::endl;
		}
		if (numEval >= numEvalMax) { continueSimplex = false; std::cout << "Max Function eval reached." << std::endl; }


		if (Report) std::cout << pBest << " " << cBest << " || " << pWorst << " " << cWorst << std::endl << std::endl;
	}

	if (c1 > c2) { pWorst = p1; cWorst = c1; pBest = p2; cBest = c2; }
	else { pWorst = p2; cWorst = c2; pBest = p1; cBest = c1; }

	return pBest;
}