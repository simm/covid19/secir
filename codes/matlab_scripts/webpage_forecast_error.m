fprintf('\nwebpage_forecast_error.m: Obtaining the performance of new case prediction...\n');

data = readtable("../data/" + sessionName + "/Germany.csv");
data.date = string(data.date);
%--------------------------------------------------------------------------
full_foldername_figures             = "../figures/" + sessionName;
full_foldername_Rt_distributions    = full_foldername_figures + "/Rt_distribution";
full_foldername_predictions         = full_foldername_figures + "/predictions";
if ~exist("../figures", 'dir');                     mkdir("../figures");                    end;
if ~exist(full_foldername_figures, 'dir');          mkdir(full_foldername_figures);         end;
if ~exist(full_foldername_Rt_distributions, 'dir'); mkdir(full_foldername_Rt_distributions);end;
if ~exist(full_foldername_predictions, 'dir');      mkdir(full_foldername_predictions);     end;
%--------------------------------------------------------------------------

remove_datapoints = 2;
prospective_days = 14;
starting_date = "2020-04-01";
ending_date   = data.date(end-remove_datapoints-prospective_days);
starting_idx = find(data.date == starting_date);
ending_idx   = find(data.date == ending_date  );



states_definition;
options = odeset('RelTol',1e-8,'AbsTol',1e-8); % Solver settings

check_median_parameter = false;
ERROR = false;

Rt = 17; % Total recovery
O  = 18;   % Observable
Ct = 19;
It = 20;
Ht = 21;
Ut = 22;
NC = 23;
%NC2= 24;
        
file_description(S)      = "Susceptible (S)";
file_description(E)      = "Exposed (E)";
file_description(Ci)     = "Carrier (Ci)";
file_description(Cr)     = "Carrier (Cr)";
file_description(Ih)     = "Infected (Ih)";
file_description(Ir)     = "Infected (Ir)";
file_description(Iprime) = "Infected (Iprime)";
file_description(Hr)     = "Hospitalized (Hr)";
file_description(Hu)     = "Hospitalized (Hu)";
file_description(Hs)     = "Hospitalized (Hs)";
        
file_description(Ur)   = "ICU (Ur)";
file_description(Ur)   = "ICU (Ud)";
file_description(Ix)   = "Infected undetected (Ix)";
file_description(Rz)   = "Recovered detected (Rz)";
file_description(Rx)   = "Recovered undetected (Rx)";
file_description(D)    = "Death toll";

file_description(Rt)   = "Recovered (Rz + Rx)";
file_description(O)    = "Cumulative reported cases";
file_description(Ct)   = "Carrier (Ci + Cr)";
file_description(It)   = "Infected (Ih + Ir)";
file_description(Ht)   = "Hospitalized";
file_description(Ut)   = "ICU";        
% file_description(NC)  = "Prospective daily new infected cases";  
file_description(NC)   = "Daily new reported cases";  
% file_description(NC2) = "New infected cases 2(daily)"; 

filename = "../results/" + sessionName + "/Germany/results.mat";

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
   
load(filename);
if ~isfile(filename)
    ERROR = true;
    error_filename_exist = "Error: File doesn't exist! "+filename;
    fprintf('%s\n', error_filename_exist);
else
    ERROR = false;        
end

if (~ERROR)
    load(filename);
    iteration = max(size(results));
end
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
   
results_matlab = cell(1, size(starting_idx:ending_idx,2));
Rt_matrix = [results.Rt];
Rt_matrix_median = median(Rt_matrix');

parfor idx = 1:size(starting_idx:ending_idx,2)
    ending_date = data.date(starting_idx + idx - 1);
    
    data_idx_start = find(data.date == ending_date);
    
    data_compare   = data.infected_newCases((data_idx_start + 1) : (data_idx_start + prospective_days));
%     data_compare   = City.Age_group_tot_newIdentified_movmean((data_idx_start + 1) : (data_idx_start + prospective_days));
    
    date_compare   = data.date ((data_idx_start + 1) : (data_idx_start + prospective_days));
    
    idx_cpp_results   = find(results(1).date == ending_date);
    Rt_mean_of_median = median(Rt_matrix_median(idx_cpp_results-6:idx_cpp_results));
    
    [q, final_condition] = get_variables_at_date_func(ending_date, results(1));        
% %     ending_date_idx = find(r1_optimum{1}.r1_optimum_R0date == ending_date);
        
	tspan = q.final_time:(q.final_time + (prospective_days+1) - 1) ;
% %     q.R1 = calculate_R1_from_Rt(tspan(1), q, Rt_imposed_value(AA));
    
    RR0_coef = [1];
        
    predictions = cell(1, size(RR0_coef,2));
    for j = 1:size(RR0_coef,2)
            out = [];
            RTs = [];
            for iter = 1:numel(results)
                t = []; y = [];
                [q, final_condition] = get_variables_at_date_func(ending_date, results(iter));  
                out.parameters(iter) = q;                

                q.R1 = calculate_R1_from_Rt(tspan(1), q, Rt_mean_of_median);
                q.Rt = calculate_Rt(tspan(1), q);
                out.R1_new(iter) = q.R1;
                out.Rt_new(iter) = q.Rt;
                RTs(iter) = q.Rt_weeklyMean;
                
                [t, y] = ode15s(@(t,y) secir_ode (t, y, q, "normal-simulation"), tspan, final_condition, options);

                out.S       (iter, :) = y(:,S)';
                out.E       (iter, :) = y(:,E)';
                
                out.Ci      (iter, :) = y(:,Ci)';
                out.Cr      (iter, :) = y(:,Cr)';
                
                out.Ih      (iter, :) = y(:,Ih)';
                out.Ir      (iter, :) = y(:,Ir)';
                out.Ix      (iter, :) = y(:,Ix)';
                out.Iprime  (iter, :) = y(:,Iprime)';
                
                out.Hr      (iter, :) = y(:,Hr)';
                out.Hu      (iter, :) = y(:,Hu)';
                out.Hs      (iter, :) = y(:,Hs)';
                
                out.Ur      (iter, :) = y(:,Ur)';
                out.Ud      (iter, :) = y(:,Ud)';
                out.Rz      (iter, :) = y(:,Rz)';
                out.Rx      (iter, :) = y(:,Rx)';                
                out.D        (iter, :) = y(:,D)';
                
                out.observable  (iter, :) = y(:,Ih)' + y(:,Ir)' + y(:,Hr)' + y(:,Hu)' + y(:,Hs)' + y(:,Ur)' + y(:,Ud)' + y(:,Rz)' + y(:,D)';
                out.new_cases   (iter, :) = [0 (out.observable  (iter, 2:end) - out.observable  (iter, 1:end-1))];
                
                out.absolute_error          (iter, :) = (sum(out.new_cases(iter, 2:end)) - sum(data_compare));
                out.absolute_percent_error	(iter, :) = (out.absolute_error   (iter, :) ./ sum(data_compare)) .* 100;
                out.date_compare = date_compare;
            end
            
%             fprintf('\nR0_coef = %d   , median RT = %d\n', RR0_coef(j), median(RTs));
            out.RTs = RTs;
            out.date = ending_date; 
            
            predictions{j} = out;
    end
    results_matlab{idx} = predictions;
end
webpage_forecast_error_figure;