fprintf('\nwebpage_federalStates.m: Comparing Rt among federal states...\n');
sessionAddress = "../results/" + sessionName + "/";
starting_date  = "2020-10-01";
%--------------------------------------------------------------------------
labels = [];
for cc = 1:max(size(city_names))
    sessionAddressFull = sessionAddress + city_names(cc) + "/results.mat";
    load(sessionAddressFull);
    ending_date = results(1).date(end-remove_final_points);
    
    districts{cc}.date          = results(1).date;
    districts{cc}.starting_idx  = find(districts{cc}.date == starting_date);
    districts{cc}.ending_idx    = find(districts{cc}.date == ending_date);
    districts{cc}.date_plot     = districts{cc}.date(districts{cc}.starting_idx : districts{cc}.ending_idx);

    districts{cc}.Rt                    = [results.Rt];
    districts{cc}.Rt_endpoint           = districts{cc}.Rt(districts{cc}.ending_idx,:);
    districts{cc}.Rt_movmean            = movmean(districts{cc}.Rt, [6 0]);
    districts{cc}.Rt_movmean_endpoint   = districts{cc}.Rt_movmean(districts{cc}.ending_idx,:);
    
    districts{cc}.Rt_medians = median(districts{cc}.Rt');
    districts{cc}.Rt_weekly_medians_endpoint = districts{cc}.Rt_medians((districts{cc}.ending_idx - 6) : (districts{cc}.ending_idx));
    districts{cc}.Rt_weekly_medians_movmean  = movmean(districts{cc}.Rt_medians, [6 0]);
    districts{cc}.Rt_weekly_medians_movmean  = districts{cc}.Rt_weekly_medians_movmean(districts{cc}.starting_idx : districts{cc}.ending_idx);

    districts{cc}.cityName = [results.cityName];
    
    if (districts{cc}.cityName == "SchleswigHolstein")       districts{cc}.cityName = "Schleswig-Holstein"; end
    if (districts{cc}.cityName == "NordrheinWestfalen")      districts{cc}.cityName = "Nordrhein-Westfalen"; end
    if (districts{cc}.cityName == "RheinlandPfalz")          districts{cc}.cityName = "Rheinland-Pfalz"; end
    if (districts{cc}.cityName == "BadenWuerttemberg")       districts{cc}.cityName = "Baden-Wuerttemberg"; end
    if (districts{cc}.cityName == "MecklenburgVorpommern")   districts{cc}.cityName = "Mecklenburg-Vorpommern"; end
    if (districts{cc}.cityName == "SachsenAnhalt")           districts{cc}.cityName = "Sachsen-Anhalt"; end
  
    % Distribution of Rt at final date
    data(:, cc)                = districts{cc}.Rt_endpoint';
    % Pool of Rt moving average at each iteration, at the final date
    data_weekly(:, cc)         = districts{cc}.Rt_movmean_endpoint;
    % Pool of median values in the last week
	data_weekly_medians(:, cc) = districts{cc}.Rt_weekly_medians_endpoint;
	labels = [labels , [districts{cc}.cityName]];
end
date = results(1).date;
starting_idx_sim = find(date == starting_date);
ending_idx_sim   = find(date == ending_date);
date             = date( starting_idx_sim : ending_idx_sim);

date(2:7:end) = "";
date(3:7:end) = "";
date(4:7:end) = "";
date(5:7:end) = "";
date(6:7:end) = "";
date(7:7:end) = "";
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% Pooled data: daily medians at the final week
f = figure('visible','off');
[x,idx] = sort( median(data_weekly_medians) );
boxplot_data = data_weekly_medians(:,idx);
boxplot_label = labels(idx);

plot([-2 20], [1 1], 'r--','Linewidth', 1); hold on;
h = boxplot(boxplot_data,'Color', 'k', 'PlotStyle','compact');
set(gca,'xtick',1:1:size(boxplot_label, 2),'xticklabel',boxplot_label);
    xtickangle(45);
    grid off;
    box on;
        bx = findobj('Tag','boxplot');
        set(findobj(bx,'Tag','Whisker'),'LineWidth',3);
        set(findobj(bx,'Tag','Box'),'LineWidth',10);
    hh=findobj(gca,'tag','Outliers'); delete(hh);
    ylabel({'Reproduction number','daily medians in a week'});
    set(gca,'FontSize',14);
    set(gcf, 'Units', 'Inches', 'Position', [0, 0, 9, 5], 'PaperUnits', 'Inches');
    title("Week ending at " + ending_date)
    h=gcf;
    print(h,"../figures/" + sessionName + "/Rt_distribution_endpoints_weeklyMedians.png", '-dpng','-r600');   
%---------------------------------------------------------------------------
%--------------- Supplement Federal states----------------------------------
%---------------------------------------------------------------------------
date = results(1).date;
starting_idx_sim = find(date == starting_date);
ending_idx_sim   = find(date == ending_date);
date             = date( starting_idx_sim : ending_idx_sim);
date(end-1:-2:1) = "";
%---------------------------------------------------------------------------
 f = figure('visible','off');
axes('Position', [0.1 0.25 0.85 0.65]); 
plot_legend = [];
line_style = ["o:", "+-.","*--", "x:", "s-.", "d-.", "p:", "h-."];
counter = 1;
for cc = 17:-1:10
    plot(districts{cc}.Rt_weekly_medians_movmean, line_style(counter), 'linewidth', 1);
    hold on;
    plot_legend = [plot_legend labels(cc)];
    counter = counter + 1;
end
leg = legend(plot_legend, 'position',[0.38 0.9 0.1 0.1], 'fontsize', 12);
set(leg,'Box','off', 'Orientation','horizontal'); leg.NumColumns = 4;
% set(leg,'Orientation','horizontal'); leg.NumColumns = 2;
set(gca,'xtick',1:numel(date),'xticklabel', date, 'fontsize', 10);
xtickangle(90);
set(0,'DefaultLegendAutoUpdate','off');
hh = plot([-2 400], [1 1], 'r--','Linewidth', 1); hold on;
xlim([0.5 (max(size(districts{cc}.Rt_weekly_medians_movmean)+0.5))]);
grid off;
box on;
xlabel({'Date'});
ylabel({'Reproduction number','7 days moving average'}, 'fontsize', 14);
set(gcf, 'Units', 'Inches', 'Position', [0, 0, 12, 5], 'PaperUnits', 'Inches');
h=gcf;
print(h,"../figures/" + sessionName + "/federalStates1_7_days_moving_average_1.png", '-dpng','-r600');
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
%---------------------------------------------------------------------------
figure;
axes('Position', [0.1 0.25 0.85 0.65]); 
plot_legend = [];
line_style = ["o:", "+-.","*--", "x:", "s-.", "d-.", "p:", "h-."];
counter = 1;
for cc = 9:-1:2
    plot(districts{cc}.Rt_weekly_medians_movmean, line_style(counter), 'linewidth', 1);
    hold on;
    plot_legend = [plot_legend labels(cc)];
    counter = counter + 1;
end
leg = legend(plot_legend, 'position',[0.38 0.9 0.1 0.1], 'fontsize', 12);
set(leg,'Box','off', 'Orientation','horizontal'); leg.NumColumns = 4;
set(gca,'xtick',1:numel(date),'xticklabel', date, 'fontsize', 10);
xtickangle(90);
set(0,'DefaultLegendAutoUpdate','off');
hh = plot([-2 400], [1 1], 'r--','Linewidth', 1); hold on;
xlim([0.5 (max(size(districts{cc}.Rt_weekly_medians_movmean)+0.5))]);
grid off;
box on;
xlabel({'Date'});
ylabel({'Reproduction number','7 days moving average'}, 'fontsize', 14);
set(gcf, 'Units', 'Inches', 'Position', [0, 0, 12, 5], 'PaperUnits', 'Inches');
h=gcf;
print(h,"../figures/" + sessionName + "/federalStates1_7_days_moving_average_2.png", '-dpng','-r600');