function R1_new = calculate_R1_from_Rt(t, parameter, Rt2)
%------------------------------------------------------------------
% R0 calculation
%------------------------------------------------------------------

Rt1 = calculate_Rt(t, parameter);
Rt1 = parameter.Rt;
% R1_old = parameter.R1;
Rt2;
R1  = parameter.R1 * Rt2 / Rt1;
parameter.R1 = R1;
tmp = calculate_Rt(t, parameter);
% parameter.r1 = R1;
% out_print = calculate_R0_CFR(t, parameter);
% out_print(1)

%Rt2 = (R1*So*alpha)/(No*R9) + (R1*So*(1 - alpha))/(No*R3) + (R1*So*chi*(1 - alpha)*(1 - m))/(No*R4) + (R1*So*beta*m*rho*(1 - alpha))/(No*R6) + (R1*So*beta*m*(1 - alpha)*(1 - rho))/(No*R4);
R1_new = R1;