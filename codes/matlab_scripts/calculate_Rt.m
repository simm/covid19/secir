function Rt = calculate_Rt(t, param)
%------------------------------------------------------------------
% R0 calculation
%------------------------------------------------------------------

So      = param.S0;
No      = param.N0_t;

rho_factor0 = param.rho * CFR_factor(t - 0, param.rho, param.theta, param.delta);
rho_factor1 = param.rho * CFR_factor(t - 1, param.rho, param.theta, param.delta);
rho_factor2 = param.rho * CFR_factor(t - 2, param.rho, param.theta, param.delta);
rho_factor3 = param.rho * CFR_factor(t - 3, param.rho, param.theta, param.delta);
rho_factor4 = param.rho * CFR_factor(t - 4, param.rho, param.theta, param.delta);
rho_factor5 = param.rho * CFR_factor(t - 5, param.rho, param.theta, param.delta);
rho_factor6 = param.rho * CFR_factor(t - 6, param.rho, param.theta, param.delta);
rho_factor7 = param.rho * CFR_factor(t - 7, param.rho, param.theta, param.delta);
rho_factor8 = param.rho * CFR_factor(t - 8, param.rho, param.theta, param.delta);
rho_factor9 = param.rho * CFR_factor(t - 9, param.rho, param.theta, param.delta);

rho_factors = [rho_factor0, rho_factor1, rho_factor2, rho_factor3, rho_factor4, rho_factor5, rho_factor6, rho_factor7, rho_factor8, rho_factor9];
rho_mean = mean(rho_factors);

% R0_1 = (R1*So*alpha)/(No*R9) + (R1*So*(1 - alpha))/(No*R3) + (R1*So*chi*(1 - alpha)*(1 - m))/(No*R4) + (R1*So*beta*m*rho*(1 - alpha))/(No*R6) + (R1*So*beta*m*(1 - alpha)*(1 - rho))/(No*R4);
% R0_2 = R1/R3;
% R0_3 = R1 * (1/R3 + 1/R4);

% R0 = [R0_1 R0_2 R0_3];
Rt = param.R1 * (So / No) * (param.alpha / param.R9 + (1.0 - param.alpha) / param.R3 ...
    + (1.0 - param.alpha) * (1.0 - param.mu) / param.R4 ...
    + param.mu * (1.0 - param.alpha) / param.tau ...
    + param.beta * param.mu * (1.0 - param.alpha) * (1.0 - rho_mean) / param.R4prime ...
    + param.beta * param.mu * rho_mean * (1.0 - param.alpha) / param.R6prime);