fprintf('\nwebpage_CSV_file.m: Exporting raw Rt values...\n');
city_requested_names = city_names;
for city_iter=1:max(size(city_requested_names))
    Rt.date = [];
    Rt.value=[];
    labels = [];
    A = [];
    Rt_table = [];
    c = cell(1,101);
    
    full_foldername_CSV   = "../figures/" + sessionName + "/Rt_rawData";
    if ~exist(full_foldername_CSV, 'dir'); mkdir(full_foldername_CSV); end;
    filename = "../results/" + sessionName + "/" + city_requested_names(city_iter) + "/results.mat";
	load(string(filename));
    Rt.date        = results(1).date;
    Rt.date        = Rt.date(1:end-remove_final_points);

    A = table(Rt.date);
    name = "date";
    A.Properties.VariableNames = name;
    c {1} = A;
	for i=1:numel(results)   
        A = results(i).Rt;
		Rt.value(i, :) = A(1:end-remove_final_points);
        A = table(Rt.value(i, :)');
        name = "Iter_" + num2str(i);
        A.Properties.VariableNames = name;
        c{i+1} = A;
    end
    t = horzcat(c{:});
    saveCityname = city_requested_names(city_iter);
    if (saveCityname == "SchleswigHolstein")       saveCityname = "Schleswig-Holstein"; end
    if (saveCityname == "NordrheinWestfalen")      saveCityname = "Nordrhein-Westfalen"; end
    if (saveCityname == "RheinlandPfalz")          saveCityname = "Rheinland-Pfalz"; end
    if (saveCityname == "BadenWuerttemberg")       saveCityname = "Baden-Wuerttemberg"; end
    if (saveCityname == "MecklenburgVorpommern")   saveCityname = "Mecklenburg-Vorpommern"; end
    if (saveCityname == "SachsenAnhalt")           saveCityname = "Sachsen-Anhalt"; end
	filename =  full_foldername_CSV + "/" + saveCityname + "_Rt.csv";
	writetable(t,filename);
end


