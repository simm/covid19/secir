function [p, final_condition] = get_variables_at_date_func(ending_date, result)
states_definition;

index_of_ending_date = find(result.date == ending_date);

% window = 7;
% result.S(:,end) = movmean(result.S(:,end),window);
% result.E(:,end) = movmean(result.E(:,end),window);
% 
% result.Cr(:,end) = movmean(result.Cr(:,end),window);
% result.Ci(:,end) = movmean(result.Ci(:,end),window);
% 
% result.Ih (:,end) = movmean(result.Ih(:,end),window);
% result.Ir(:,end) = movmean(result.Ir(:,end),window);
% 
% result.Hu (:,end) = movmean(result.Hu (:,end),window);
% result.Hr (:,end) = movmean(result.Hr (:,end),window);
% result.Hs(:,end) = movmean(result.Hs(:,end),window);
% result.Ur(:,end) = movmean(result.Ur(:,end),window);
% result.Ud(:,end) = movmean(result.Ud(:,end),window);
% result.D(:,end) = movmean(result.D(:,end),window);
% 
% result.Rx(:,end) = movmean(result.Rx(:,end),window);
% result.Rz(:,end) = movmean(result.Rz(:,end),window);
% 
% result.Ix(:,end) = movmean(result.Ix(:,end),window);
% result.Iprime(:,end) = movmean(result.Iprime(:,end),window);

    final_condition(S)  = result.S   (index_of_ending_date, end);
    final_condition(E)  = result.E   (index_of_ending_date, end);
    
    final_condition(Cr)  = result.Cr (index_of_ending_date, end);
    final_condition(Ci)  = result.Ci (index_of_ending_date, end);
    
    final_condition(Ih)  = result.Ih (index_of_ending_date, end);
    final_condition(Ir)  = result.Ir (index_of_ending_date, end);
    
    final_condition(Hu)  = result.Hu (index_of_ending_date, end);
    final_condition(Hr)  = result.Hr (index_of_ending_date, end);
    final_condition(Hs)  = result.Hs (index_of_ending_date, end);
    
    final_condition(Ur)  = result.Ur (index_of_ending_date, end);
    final_condition(Ud)  = result.Ud (index_of_ending_date, end);
    
    final_condition(D)   = result.D  (index_of_ending_date, end);
    final_condition(Rx)  = result.Rx (index_of_ending_date, end);
    final_condition(Rz)  = result.Rz (index_of_ending_date, end);
    
    final_condition(Ix)      = result.Ix (index_of_ending_date, end);
    final_condition(Iprime)  = result.Iprime (index_of_ending_date, end);
    
    
    
    
    
p.R1            = result.R1    (index_of_ending_date);
p.Rt            = result.Rt    (index_of_ending_date);
p.final_time    = result.time  (index_of_ending_date, end);
p.R2            = result.parameter.R2;
p.R3            = result.parameter.R3;
p.R4prime       = result.parameter.R4prime;
p.R5            = result.parameter.R5;
p.R6prime       = result.parameter.R6prime;
p.R7            = result.parameter.R7;
p.R8            = result.parameter.R8;
p.R9            = result.parameter.R9;
p.R10           = result.parameter.R10;

p.alpha         = result.parameter.alpha;
p.beta          = result.parameter.beta;
p.rho           = result.parameter.rho;
p.theta         = result.parameter.theta;
p.delta         = result.parameter.delta;
p.mu            = result.parameter.mu;
p.tau           = result.parameter.tau;
p.chi           = 1;

p.CFR_H         = result.parameter.CFR_H;
p.CFR_L         = result.parameter.CFR_L;
p.CFR_k         = result.parameter.CFR_k;
p.CFR_t0        = result.parameter.CFR_t0;

p.N0 = 83019213;
p.S0 = result.S(index_of_ending_date, 1);
p.N0_t = p.N0 - result.D(index_of_ending_date, 1);
% p.R6 = 1/((1/p.R6prime) + (1/p.tau));
% p.R4 = 1/((1/p.R4prime) + (1/p.tau));
p.R6 = result.parameter.R6;
p.R4 = result.parameter.R4;


p.enable_time_varying_mu        = result.parameter.enable_time_varying_mu;
p.enable_time_varying_rho       = result.parameter.enable_time_varying_rho;
p.enable_time_varying_theta     = result.parameter.enable_time_varying_theta;
p.enable_time_varying_delta     = result.parameter.enable_time_varying_delta;

p.final_time = result.time(index_of_ending_date, end);
p.final_date = result.date(index_of_ending_date);

p.Rt_weeklyMean = mean(result.Rt    (index_of_ending_date-6:index_of_ending_date));

% parameter.r1
