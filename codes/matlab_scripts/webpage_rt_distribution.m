fprintf('\nwebpage_rt_distribution.m: Plotting time-evolution of Rt for individual states...\n');
%--------------------------------------------------------------------------
full_foldername_figures             = "../figures/" + sessionName;
full_foldername_Rt_distributions    = full_foldername_figures + "/Rt_distribution";
full_foldername_predictions         = full_foldername_figures + "/predictions";

if ~exist("../figures", 'dir');                     mkdir("../figures");                    end;
if ~exist(full_foldername_figures, 'dir');          mkdir(full_foldername_figures);         end;
if ~exist(full_foldername_Rt_distributions, 'dir'); mkdir(full_foldername_Rt_distributions);end;
if ~exist(full_foldername_predictions, 'dir');      mkdir(full_foldername_predictions);     end;
%--------------------------------------------------------------------------
city_requested_names    = ["Germany", "SchleswigHolstein", "Hamburg", "Niedersachsen", "Bremen", "NordrheinWestfalen" ,"Hessen" ,"RheinlandPfalz" ,"BadenWuerttemberg", "Bayern", "Saarland", "Berlin", "Brandenburg", "MecklenburgVorpommern", "Sachsen", "SachsenAnhalt", "Thueringen"];
% city_requested_names    = ["Berlin"];
%--------------------------------------------------------------------------
% Determine where your m-file's folder is.
folder = fileparts(which(mfilename)); 
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));

check_median_parameter = false;
ERROR = false;
for city_iter = 1:max(size(city_requested_names))
    city_name = city_requested_names(city_iter);
	filename = "../results/" + sessionName + "/" + city_name + "/results.mat";	
    if ~isfile(filename)
        ERROR = true;
        error_filename_exist = "Error: File doesn't exist! "+filename;
        fprintf('%s\n', error_filename_exist);
    else
        ERROR = false;        
	end
%--------------------------------------------------------------------------
if (~ERROR)
    load(filename);

    starting_date = "2020-10-01";
    idx_starting_date = find(results(1).date == starting_date);
    Dates = results(1).date';    
    R0 = [];
    R_movingAverage = [];
    
    Rts = [results.Rt]';
    Rts_median = median(Rts);
    R_movingAverage = movmean(Rts', [6 0])';    
    R_movingAverage_meadians = movmean(median(Rts), [6 0]);
    
    R0                  = Rts                       (: , idx_starting_date : (end-remove_final_points));
    R0_movmean          = R_movingAverage           (: , idx_starting_date : (end-remove_final_points));
    R0_movmean_medians  = R_movingAverage_meadians  (    idx_starting_date : (end-remove_final_points));
    Dates               = Dates                     (    idx_starting_date : (end-remove_final_points));
    %----------------------------------------------------------------------
    DeviceColors = {'k'};
    GroupedData = {R0}; % Plotting code is written to show multiple boxplots. In that case, GroupedData = {data1 data2 ...}
    N = numel(GroupedData);
    delta = linspace(-.15,.15,N); % define offsets to distinguish plots
    width = 1; % small width to avoid overlap
    cmap = hsv(N); % colormap
    legWidth = 0; % make room for legend    
    MaxY = 0;
    MinY=1e300;
    for j=1:N
        MaxY = max(MaxY, max(max(GroupedData{j})));
        MinY = min(MinY, min(min(GroupedData{j})));
	end
    f = figure('visible','off');
    %-----------------------------------------
%     my_axes = axes('Position',[.02 .2 .85 .7]);
    my_axes = axes('Position',[.02 .2 .87 .7]);
    %-----------------------------------------
    hold on;
    for ii=1:N %// better not to shadow i (imaginary unit)
        labels = Dates; %// center plot: use real labels
        h = boxplot(GroupedData{ii},'Color', DeviceColors{ii}, 'PlotStyle','compact', 'width', 0.2, 'position',1:numel(labels),  'labels',labels);
        %// plot filled boxes with specified positions, widths, labels
        plot(1:1:size(labels, 2), R0_movmean_medians,'color','r', 'linewidth', 3); %// dummy plot for legend
    end
    
    legend_text = "7 days average: final Rt = " + num2str(R0_movmean_medians(end), '%.2f');
    set(0,'DefaultLegendAutoUpdate','off');
 	legend(legend_text, 'fontsize', 14);    
    set(gca,'FontSize',14);
    labels(end-1:-2:1) = "";
    set(gca,'xtick',1:1:size(labels, 2),'xticklabel',labels(1:1:end))
    xtickangle(90);
    grid on;
    box on;


    set(gcf, 'Units', 'Inches', 'Position', [0, 0, 12, 5], 'PaperUnits', 'Inches')
    hh=findobj(gca,'tag','Outliers'); delete(hh);
    box = findobj(gca,'tag','Box');
    prctls = [];
    for i=1:numel(box)
        prctls(i,1:2) = unique(box(i).YData);
    end
    MaxY = max(max(prctls));
    ylim([-0.1 1.37*MaxY]);
    
    set(0,'DefaultLegendAutoUpdate','off');
    hhh = plot ([0, 200], [1, 1], '--', 'color', 'r', 'linewidth', 1); uistack(hhh,'bottom');
    ax = gca; % Get handle to current axes.
    ax.GridAlpha = 1;  % Make grid lines less transparent.
    ax.GridColor = [0.831, 0.831, 0.831]; % Dark Green.
    for i=0.25:0.25:15
         hhh = plot ([0, 200], [i, i], '--', 'color', [0.831, 0.831, 0.831]);
%         alpha(hhh,0.1)
         uistack(hhh,'bottom');
    end
    
    
    %---------------------------------
    bx = findobj('Tag','boxplot');
    set(findobj(bx,'Tag','Whisker'),'LineWidth',3);
    set(findobj(bx,'Tag','Box'),'LineWidth',1);
    hh=findobj(gca,'tag','Outliers'); delete(hh);
    %---------------------------------
    a = get(gca,'XTickLabel');  
    set(gca,'XTickLabel',a,'fontsize',9)%,'FontWeight','bold')
    %----------------------------------------------------------------------
%     ax1=gca
%     ax1_old_pos=ax1.Position; % [ax ay bx by] 2 diagonal points defining the graph area
%     Yscale_bottom =1.6;
%     Yscale_top    =0.93;
%     ax1.Position=[ax1_old_pos(1) ax1_old_pos(2)*Yscale_bottom ax1_old_pos(3) ax1_old_pos(4)*Yscale_top];
    %----------------------------------------------------------------------
%     text(0.35, -0.23, 'Ending date of time-window', 'fontsize', 14, 'Units','Normalized');
    xlabel({" "," ", " ","Ending date of time-window"}, 'fontsize', 14);
    final_Rt_text = "Final Rt = " + num2str(median(R0(:, end)), '%.3f');
%     annotation('textbox', [0.26, 0.82, 0.1, 0.1], 'fontsize', 14, 'BackgroundColor', "w", 'String', final_Rt_text)
    ylabel('Reproduction number', 'fontsize', 14);
    %----------------------------------------------------------------------    
    if (city_name == "SchleswigHolstein")       city_name = "Schleswig-Holstein"; end
    if (city_name == "NordrheinWestfalen")      city_name = "Nordrhein-Westfalen"; end
    if (city_name == "RheinlandPfalz")          city_name = "Rheinland-Pfalz"; end
    if (city_name == "BadenWuerttemberg")       city_name = "Baden-Wuerttemberg"; end
    if (city_name == "MecklenburgVorpommern")   city_name = "Mecklenburg-Vorpommern"; end
    if (city_name == "SachsenAnhalt")           city_name = "Sachsen-Anhalt"; end
    filename_figure_png = full_foldername_Rt_distributions + "/" + city_name + ".png";
    %----------------------------------------------------------------------
    title(city_name, 'fontsize', 14);
    %----------------------------------------------------------------------
    hh=findobj(gca,'tag','Outliers'); delete(hh);
    %----------------------------------------------------------------------
    h = gcf;    print(h,filename_figure_png, '-dpng','-r400');
end
end