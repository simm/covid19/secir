%--------------------------------------------------------------------------
% States definition
%--------------------------------------------------------------------------
S  = 1;      v.S = S;
E  = 2;      v.E = E;
Ci = 3;      v.Ci = Ci;
Cr = 4;      v.Cr = Cr;
Ix = 5;      v.Ix = Ix;
Iprime = 6;  v.Iprime = Iprime;
Ir = 7;      v.Ir = Ir;
Ih = 8;      v.Ih = Ih;
Hr = 9;      v.Hr = Hr;
Hu = 10;     v.Hu = Hu;
Hs = 11;     v.Hs = Hs;
Ur = 12;     v.Ur = Ur;
Ud = 13;     v.Ud = Ud;
D  = 14;     v.D  = D;
Rx = 15;     v.Rx = Rx;
Rz = 16;     v.Rz = Rz;
