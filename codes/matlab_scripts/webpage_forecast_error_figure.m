f = figure('visible','off');
city_name = "Germany";
labels_complete = data.date(starting_idx:(ending_idx+prospective_days));
for i=1:numel(results_matlab)
    labels(i) = results_matlab{1,i}{1,1}.date;
    data_boxplot_absolute_percent_error (:, i) = results_matlab{1,i}{1,1}.absolute_percent_error;
end
%---------------------------
Rt_ending_date   = data.date(ending_idx + prospective_days);
Rt_starting_date = data.date(starting_idx);
Rt_ending_date_idx   = find(results(1).date == Rt_ending_date);
Rt_starting_date_idx = find(results(1).date == Rt_starting_date);
% for p=1:numel(results)    
%     A = [results(p).r1_optimum_R0value.R0];
%     Rts_temp(p,:) = A(Rt_starting_date_idx : Rt_ending_date_idx);    
% end
Rts = [results.Rt]';
Rts_temp = Rts(:, Rt_starting_date_idx : Rt_ending_date_idx);
axes('Position', [0.1 0.715 0.8 0.2]); 
boxplot(Rts_temp, 'position', 1:max(size(Rts_temp)), 'whisker', 21); hold on;
    set(findobj(gca,'type','line'),'linew',1.5);
    set(findobj(gca,'type','line'),'LineStyle','-');
    xlim([0 max(size(labels_complete))+1]);
    set(gca,'xtick',1:1:max(size(labels_complete)),'xticklabel',strings(1,max(size(labels_complete))+1));
    grid on;
    ylim([0 2.5]);
    ylabel('Reproduction number', 'fontsize', 12);
    title(city_name, 'fontsize', 14);
    set(gca,'FontSize',11);
    
    hhh = plot ([0, 400], [1, 1], '--', 'color', 'r', 'linewidth', 1); uistack(hhh,'bottom');
    ax = gca; % Get handle to current axes.
    ax.GridAlpha = 1;  % Make grid lines less transparent.
    ax.GridColor = [0.831, 0.831, 0.831]; % Dark Green.
    for i=0.25:0.25:15
         hhh = plot ([0, 200], [i, i], '--', 'color', [0.831, 0.831, 0.831]);
%         alpha(hhh,0.1)
         uistack(hhh,'bottom');
    end
    
%---------------------------

axes('Position', [0.1 0.2 0.8 0.5]); 
labels_complete(end-1:-7:1) = "";
labels_complete(end-2:-7:1) = "";
labels_complete(end-3:-7:1) = "";
labels_complete(end-4:-7:1) = "";
labels_complete(end-5:-7:1) = "";
labels_complete(end-6:-7:1) = "";
% labels_complete(end) = Rt_ending_date;
boxplot(data_boxplot_absolute_percent_error, 'position', 1:max(size(data_boxplot_absolute_percent_error)), 'whisker', 21); hold on;
    set(gca,'xtick',1:1:max(size(labels_complete)),'xticklabel',labels_complete(1:1:end));
    set(findobj(gca,'type','line'),'linew',1.5);
    set(findobj(gca,'type','line'),'LineStyle','-');    
    xlim([0 max(size(labels_complete))+1]);
    xtickangle(90);
    xlabel("Date");
    ylabel({"Prediction error (%)"});
patch([-1 -1 300 300], [25   300  300  25],[0 1 0],'FaceAlpha',0.2);
patch([-1 -1 300 300], [-25 -300 -300 -25],[1 0 0],'FaceAlpha',0.2);

% annotation('textbox', [0.46, 0.58, 0.1, 0.1], 'fontsize', 12, ...
%            'BackgroundColor', "w", 'String', ...
%            "Prediction error of cumulative new registered cases within 2 weeks")
set(gca,'FontSize',11);
grid on; 


% title("Prediction error of cumulative new registered cases within 2 weeks")
    
%         ax1=gca
%         ax1_old_pos=ax1.Position; % [ax ay bx by] 2 diagonal points defining the graph area
%         Yscale_bottom =1.2;
%         Yscale_top    =0.93;
%         ax1.Position=[ax1_old_pos(1) ax1_old_pos(2)*Yscale_bottom ax1_old_pos(3) ax1_old_pos(4)*Yscale_top];

set(gcf, 'Units', 'Inches', 'Position', [0, 0, 12, 8], 'PaperUnits', 'Inches');

% ylim([15 100])
% title("14 days prediction: Rt")
h = gcf;


filename_figure_png = full_foldername_predictions + "/" + city_name + "_prediciton_performance.png";
print(gcf, filename_figure_png, '-dpng','-r600');