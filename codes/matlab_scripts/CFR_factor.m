function factor = CFR_factor(t, rho, theta, delta)

CFR_current = rho * theta * delta;

mu = 1.0;
if (mu == 1.0)
    CFR_H  = 0.123768;
    CFR_L  = 0.007659;
    CFR_k  = 0.167916;
    CFR_t0 = 89.330331;
end

CFR_to_be = CFR_H - ( CFR_H - CFR_L ) * (1.0 / (1.0 + exp(-CFR_k * (t - CFR_t0 ))));

fold_change = CFR_to_be / CFR_current;
factor = fold_change;