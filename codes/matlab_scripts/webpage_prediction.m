fprintf('\nwebpage_prediction.m: Prospective analysis - weekly new reported cases...\n');
city_name = "Germany";
prospective_days = 21;
%--------------------------------------------------------------------------
full_foldername_figures             = "../figures/" + sessionName;
full_foldername_Rt_distributions    = full_foldername_figures + "/Rt_distribution";
full_foldername_predictions         = full_foldername_figures + "/predictions";

if ~exist("../figures", 'dir');                     mkdir("../figures");                    end;
if ~exist(full_foldername_figures, 'dir');          mkdir(full_foldername_figures);         end;
if ~exist(full_foldername_Rt_distributions, 'dir'); mkdir(full_foldername_Rt_distributions);end;
if ~exist(full_foldername_predictions, 'dir');      mkdir(full_foldername_predictions);     end;
%--------------------------------------------------------------------------
states_definition;
options = odeset('RelTol',1e-8,'AbsTol',1e-8); % Solver settings

Rt = 17; % Total recovery
O  = 18;   % Observable
Ct = 19;
It = 20;
Ht = 21;
Ut = 22;
NC = 23;
NC2= 24;
        
file_description(S)      = "Susceptible (S)";
file_description(E)      = "Exposed (E)";
file_description(Ci)     = "Carrier (Ci)";
file_description(Cr)     = "Carrier (Cr)";
file_description(Ih)     = "Infected (Ih)";
file_description(Ir)     = "Infected (Ir)";
file_description(Iprime) = "Infected (Iprime)";
file_description(Hr)     = "Hospitalized (Hr)";
file_description(Hu)     = "Hospitalized (Hu)";
file_description(Hs)     = "Hospitalized (Hs)";
        
file_description(Ur)     = "ICU (Ur)";
file_description(Ur)     = "ICU (Ud)";
file_description(Ix)     = "Infected undetected (Ix)";
file_description(Rz)     = "Recovered detected (Rz)";
file_description(Rx)     = "Recovered undetected (Rx)";
file_description(D)      = "Death toll";

file_description(Rt)   = "Recovered (Rz + Rx)";
file_description(O)    = "Cumulative reported cases";
file_description(Ct)   = "Carrier (Ci + Cr)";
file_description(It)   = "Infected (Ih + Ir)";
file_description(Ht)   = "Hospitalized";
file_description(Ut)   = "ICU";        

file_description(NC)   = "Daily new reported cases";  
file_description(NC2)  = "New infected cases 2(daily)";  
%--------------------------------------------------------------------------
filename            = "../results/" + sessionName + "/" + city_name + "/results.mat";	
if ~isfile(filename)
    ERROR = true;
    error_filename_exist = "Error: File doesn't exist! "+filename;
    fprintf('%s\n', error_filename_exist);
else
    ERROR = false;        
end
if (~ERROR)
    load(filename);
    iteration = max(size(results));
end
%--------------------------------------------------------------------------
date = results(1).date;
ending_date = date(end - remove_final_points);
ending_idx  = find(date == ending_date);
RT                    = [results.Rt];
RT_median             = median(RT')';
RT_median_endpoint    = RT_median(ending_idx);
RT_endpoint           = RT(ending_idx,:);
RT_movmean            = movmean(RT_median, [6 0]);
RT_movmean_endpoint   = RT_movmean(ending_idx,:);
%--------------------------------------------------------------------------    
[q, final_condition] = get_variables_at_date_func(ending_date,results(1));
tspan = q.final_time : ( q.final_time + prospective_days );
Rt_imposed_value   = [RT_movmean_endpoint];
predictions = cell(1, size(Rt_imposed_value,2));

parfor j = 1:size(Rt_imposed_value,2)
	out = [];
	RTs = [];
	for iter = 1:iteration
        t = []; y = [];
        [q, final_condition] = get_variables_at_date_func(ending_date, results(iter));
%                 
        AA = j;
        if (Rt_imposed_value(AA) ~= 1000) % An exception rule. The value of 1000 skips Rt imposition.
            q.R1 = calculate_R1_from_Rt(tspan(1), q, Rt_imposed_value(AA));
        end
        [t, y] = ode15s(@(t,y) secir_ode (t, y, q, "normal-simulation"), tspan, final_condition, options);
% 
        out.S       (iter, :) = y(:,S)';
        out.E       (iter, :) = y(:,E)';
                
        out.Ci      (iter, :) = y(:,Ci)';
        out.Cr      (iter, :) = y(:,Cr)';
                
        out.Ih      (iter, :) = y(:,Ih)';
        out.Ir      (iter, :) = y(:,Ir)';
        out.Ix      (iter, :) = y(:,Ix)';
        out.Iprime  (iter, :) = y(:,Iprime)';
                
        out.Hr      (iter, :) = y(:,Hr)';
        out.Hu      (iter, :) = y(:,Hu)';
        out.Hs      (iter, :) = y(:,Hs)';
                
        out.Ur      (iter, :) = y(:,Ur)';
        out.Ud      (iter, :) = y(:,Ud)';
        out.Rz      (iter, :) = y(:,Rz)';
        out.Rx      (iter, :) = y(:,Rx)';                
        out.D       (iter, :) = y(:,D)';               
                
        out.observable  (iter, :) = y(:,Ih)' + y(:,Ir)' + y(:,Hr)' + y(:,Hu)' + y(:,Hs)' + y(:,Ur)' + y(:,Ud)' + y(:,Rz)' + y(:,D)';
        out.new_cases   (iter, :) = [0 (out.observable  (iter, 2:end) - out.observable  (iter, 1:end-1))];
    end
    predictions{j} = out;
end
         
for j=1:(size(Rt_imposed_value,2))
    % Sampling from the results. 1: daily, 7:weekly
    samplingFreq = 1;
    window = 1 : samplingFreq : size(tspan, 2);
    Groupdata_S{j}      = predictions{j}.S      (:,window);
	Groupdata_E{j}      = predictions{j}.E      (:,window);    
    Groupdata_Ci{j}     = predictions{j}.Ci     (:,window);
    Groupdata_Cr{j}     = predictions{j}.Cr     (:,window);    
    Groupdata_Ih{j}     = predictions{j}.Ih     (:,window);
    Groupdata_Ir{j}     = predictions{j}.Ir     (:,window);
    Groupdata_Ix{j}     = predictions{j}.Ix     (:,window);
    Groupdata_Iprime{j} = predictions{j}.Iprime (:,window);    
    Groupdata_Hr{j}     = predictions{j}.Hr     (:,window);
    Groupdata_Hu{j}     = predictions{j}.Hu     (:,window);
    Groupdata_Hs{j}     = predictions{j}.Hs     (:,window);    
    Groupdata_Ur{j}     = predictions{j}.Ur     (:,window);
    Groupdata_Ud{j}     = predictions{j}.Ud     (:,window);    
    Groupdata_Rx{j}     = predictions{j}.Rx     (:,window);
    Groupdata_Rz{j}     = predictions{j}.Rz     (:,window);                
    Groupdata_D{j}      = predictions{j}.D      (:,window);    
          
    Groupdata_Rt{j} = predictions{j}.Rx(:,window)  + predictions{j}.Rz(:,window);
    Groupdata_Ct{j} = predictions{j}.Ci(:,window)  + predictions{j}.Cr(:,window);
    Groupdata_It{j} = predictions{j}.Ih(:,window)  + predictions{j}.Ir(:,window);
    Groupdata_Ht{j} = predictions{j}.Hr(:,window)  + predictions{j}.Hu(:,window) + predictions{j}.Hs(:,window);
    Groupdata_Ut{j} = predictions{j}.Ur(:,window)  + predictions{j}.Ud(:,window);
    Groupdata_O{j}  = predictions{j}.observable  (:,window);
    Groupdata_NC{j} = predictions{j}.new_cases   (:,window);
end
GroupedData_total=[];
GroupedData_total{O}  = Groupdata_O;	mLabels(O)  = file_description(O);
GroupedData_total{S}  = Groupdata_S;	mLabels(S)  = file_description(S);
GroupedData_total{E}  = Groupdata_E;	mLabels(E)  = file_description(E);
        
GroupedData_total{Ci}  = Groupdata_Ci;	mLabels(Ci)  = file_description(Ci);
GroupedData_total{Cr}  = Groupdata_Cr;	mLabels(Cr)  = file_description(Cr);
        
GroupedData_total{Ih}  = Groupdata_Ih;	mLabels(Ih)  = file_description(Ih);
GroupedData_total{Ir}  = Groupdata_Ir;	mLabels(Ir)  = file_description(Ir);
GroupedData_total{Ix}  = Groupdata_Ix;	mLabels(Ix)  = file_description(Ix);
GroupedData_total{Iprime}  = Groupdata_Iprime;	mLabels(Iprime)  = file_description(Iprime);
        
GroupedData_total{Hr}  = Groupdata_Hr;	mLabels(Hr)  = file_description(Hr);
GroupedData_total{Hu}  = Groupdata_Hu;	mLabels(Hu)  = file_description(Hu);
GroupedData_total{Hs}  = Groupdata_Hs;	mLabels(Hs)  = file_description(Hs);
        
GroupedData_total{Ur}  = Groupdata_Ur;	mLabels(Ur)  = file_description(Ur);
GroupedData_total{Ud}  = Groupdata_Ud;	mLabels(Ud)  = file_description(Ud);
GroupedData_total{Rz} = Groupdata_Rz;   mLabels(Rz) = file_description(Rz);
GroupedData_total{Rx} = Groupdata_Rx;   mLabels(Rx) = file_description(Rx);
GroupedData_total{Rt} = Groupdata_Rt;   mLabels(Rt) = file_description(Rt);
        
GroupedData_total{D}  = Groupdata_D;    mLabels(D)  = file_description(D);        
        
GroupedData_total{Ct} = Groupdata_Ct;   mLabels(Ct) = file_description(Ct);
GroupedData_total{It} = Groupdata_It;   mLabels(It) = file_description(It);
GroupedData_total{Ht} = Groupdata_Ht;   mLabels(Ht) = file_description(Ht);
GroupedData_total{Ut} = Groupdata_Ut;   mLabels(Ut) = file_description(Ut);
GroupedData_total{NC} = Groupdata_NC;   mLabels(NC) = file_description(NC);        
webpage_prediction_figure;