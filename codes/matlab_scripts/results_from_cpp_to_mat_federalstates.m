fprintf('\nresults_from_cpp_to_mat_federalstates.m: Reading CSV files and saving to .mat...\n');
sessionAddress = "../results/" + sessionName + "/";
for cc = 1:max(size(city_names))
    sessionAddressFull = sessionAddress + city_names(cc) + "/";
    unzip(convertStringsToChars(sessionAddressFull + "results.zip"), convertStringsToChars(sessionAddressFull + "tmp/"))
    saveResults = sessionAddressFull + "results";

    iterations = 100;
    results = [];
    results.cityName = city_names(cc);
    results.cityName
    
    for i = 1:iterations
        filename = sessionAddressFull + "tmp/" + city_names(cc) + "_date_iter_" + num2str(i-1) + ".csv"; A = readtable(filename); results(i).date = string(A.Var1);
        filename = sessionAddressFull + "tmp/" + city_names(cc) + "_time_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);results(i).time = A;

        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Rt_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Rt = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Rt_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Rt = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_R1_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).R1 = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_S_iter_"  + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).S  = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_E_iter_"  + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).E  = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Cr_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Cr = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Ci_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Ci = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Ih_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Ih = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Ir_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Ir = A;    
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Hu_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Hu = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Hr_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Hr = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Hs_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Hs = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Ur_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Ur = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Ud_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Ud = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_D_iter_"  + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).D  = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Rz_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Rz = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Rx_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Rx = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Ix_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Ix = A;
        %filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_mu_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).mu_t = A;
        %filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_rho_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename); results(i).rho_t = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_Iprime_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).Iprime = A;

        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_fitting_cost_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  results(i).fitting_cost = A;
        filename = sessionAddressFull + "tmp/"+ city_names(cc) + "_parameters_iter_" + num2str(i-1) + ".csv"; A = readmatrix(filename);  parameters_cpp(i,:) = A';
        results(i).parameter = readtable(filename);
    end
    save(saveResults, "results")
    deleteFolder = sessionAddressFull + "tmp/";
    if exist(deleteFolder, 'dir')
           [status, message, messageid] = rmdir(convertStringsToChars(deleteFolder), 's');
    end
end