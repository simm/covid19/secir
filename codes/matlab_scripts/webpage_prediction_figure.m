data = readtable("../data/" + sessionName + "/Germany.csv");
GermanyPop = 83166711;
incidenceFactor = 100000 / GermanyPop;
idx_data = find(data.date==ending_date);
weeklyIncidence_data = sum(data.infected_newCases(idx_data-6:idx_data)) * incidenceFactor;

DeviceColors = [[0,0,0]; [1,0,0]];
ylim_setting = [9000 17000];
t1 = datetime(date(end-remove_final_points), 'Format','yyyy-MM-dd');
t2 = datetime(date(end-remove_final_points), 'Format','yyyy-MM-dd') + prospective_days;
dates_plot = string(t1:t2);

list_of_vars = [NC];
for i = 1:size(list_of_vars,2)
    panelNum = list_of_vars(i);
    f = figure('visible','on');
    GroupedData = GroupedData_total{panelNum};    

    out = [];
    weeklyIncidence = [];
    labels = [];
    MaxY = 0; MinY = 1e300;
    clear labels;
    labels = "";
    for n1 = 1:numel(GroupedData)        
        counter = 1;
        for n2 = 1:7:prospective_days
            A = GroupedData{n1};
            weeklyIncidence (:,counter) = sum(A(:,n2+1:n2+1+6),2) * incidenceFactor;            
            labels(counter) = num2str(counter);
            MaxY = max(MaxY, max(max(weeklyIncidence)));
            MinY = min(MinY, min(min(weeklyIncidence)));            
            counter = counter + 1;
        end
        out{n1} = weeklyIncidence;
    end
    MaxY = max(MaxY, weeklyIncidence_data);
    MinY = min(MinY, weeklyIncidence_data);
    
    
    GroupedData = out;
    N = numel(GroupedData);
    if (N > 1)
        delta = linspace(-.1,.1,N); %// define offsets to distinguish plots
    else
        delta = linspace(0,0,N); %// define offsets to distinguish plots
    end
    legWidth = 0; %// make room for legend
    set(gca,'FontSize',12);
    hold on;
    for ii=1:N %// better not to shadow i (imaginary unit)
        h=boxplot(GroupedData{ii}, 'width', 0.2, ...
                  'position',(1:size(GroupedData{ii},2)) + delta(ii));
%         plot(NaN,1,'color',DeviceColors(ii, :), 'linewidth', 3); %// dummy plot for legend
    end    
    set(gca,'xtick', 1:1:size(GroupedData{ii},2) ,'xticklabel', labels);
    ylim([0.98*MinY 1.02*MaxY]);
    xlabel({"# weeks later"});
    ylabel("7-days incidence per 100K");
    title({"Prospective analysis from " + ending_date + "  (Rt=" + num2str(RT_movmean_endpoint, '%.2f') + ")"}, 'fontsize', 14);
	%---------------------------------
	bx = findobj('Tag','boxplot');
	set(findobj(bx,'Tag','Whisker'),'LineWidth',1);
	set(findobj(bx,'Tag','Box'),'LineWidth',1);
	hh=findobj(gca,'tag','Outliers'); delete(hh);
    set(findobj(gca,'type','line'),'linew',1);
    set(findobj(gca,'type','line'),'LineStyle','-');    
    
    ax1=gca;
    ax1_old_pos=ax1.Position;
    Yscale_bottom = 1.6;
    Yscale_top    = 0.8;
    ax1.Position=[ax1_old_pos(1) ax1_old_pos(2)*Yscale_bottom ax1_old_pos(3) ax1_old_pos(4)*Yscale_top];
    %---------------------------------    
    plot([-1 5], ones(1,2)*weeklyIncidence_data, 'r--', 'linewidth', 2); hold on; %// dummy plot for legend
%     legend("Data for " + ending_date, "location", "best")
%     txt = '\leftarrow Data for ' + ending_date;
%     text(2.5, weeklyIncidence_data * 1.03,txt, 'FontSize', 12)
    %[xt yt] = ds2nfu(2.5, weeklyIncidence_data * 1.06);
    %[xt yt] = ds2nfu(2.5, weeklyIncidence_data * 1.02);
    [xt yt] = ds2nfu(2.5, weeklyIncidence_data * 0.95);
    % head
    %[xh yh] = ds2nfu(2.5, weeklyIncidence_data * 1.01);
    %[xh yh] = ds2nfu(2.5, weeklyIncidence_data * 1.005);
    [xh yh] = ds2nfu(2.5, weeklyIncidence_data * 0.995);
    annotation('textarrow',[xt xh],[yt yh],'String'," Data: " + ending_date + " ",'FontSize',13,'Linewidth',2)
    
    set(gcf, 'Units', 'Inches', 'Position', [0, 0, 9, 5], 'PaperUnits', 'Inches');
    ax = gca;    
    ax.YAxis.Exponent = 0;
    box on; grid on;

    filename_figure_png = full_foldername_predictions + "/" + city_name + "_predicitons_weekly_new_cases.png";
    print(gcf, filename_figure_png, '-dpng','-r600');
end
