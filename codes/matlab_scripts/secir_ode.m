function dydt = secir_ode(t, ...
                          x, ...
                          p, ...
                          status)   % If lockdown-simulation
N    = 16;
dydt = zeros(N,1);

states_definition;


NI = 1/p.N0;

factor = CFR_factor(t, p.rho, p.theta, p.delta);
rho_t   = p.rho   * factor;

%--------------------------------------------------------------------------
CC1 = p.R1 * NI;
Carrier_tot  =  x(Ci) + x(Cr);

%--------------------------------------------------------------------------
dydt(S)  = -CC1 * (Carrier_tot  + p.chi * x(Ix) + p.beta * x(Ir) + p.beta * x(Ih) + x(Iprime)) * x(S);
%--------------------------------------------------------------------------
dydt(E)  =  CC1 * (Carrier_tot  + p.chi * x(Ix) + p.beta * x(Ir) + p.beta * x(Ih) + x(Iprime)) * x(S) - p.R2 * x(E);            
%--------------------------------------------------------------------------
dydt(Ci) = (1.0 - p.alpha)* p.R2 * x(E) - p.R3 * x(Ci);
dydt(Cr) = p.alpha * p.R2 * x(E) - p.R9 * x(Cr);
%--------------------------------------------------------------------------
dydt(Iprime) = p.mu * p.R3 * x(Ci) - p.tau * x(Iprime);
dydt(Ix) = (1 - p.mu) * p.R3 * x(Ci) - p.R4 * x(Ix);
%--------------------------------------------------------------------------
dydt(Ih) = rho_t * p.tau * x(Iprime) - p.R6prime * x(Ih); 
dydt(Ir) = (1 - rho_t) * p.tau * x(Iprime) - p.R4prime * x(Ir); 
%--------------------------------------------------------------------------
dydt(Hu) = p.theta * p.R6prime * x(Ih) - p.R7 * x(Hu);
dydt(Hr) = (1.0 - p.theta) * p.R6prime * x(Ih) - p.R5 * x(Hr);
dydt(Hs) = p.R8 * x(Ur) - p.R5 * x(Hs);
%--------------------------------------------------------------------------
dydt(Ud) = p.delta * p.R7 * x(Hu) - p.R10 * x(Ud);
dydt(Ur) = (1.0 - p.delta) * p.R7 * x(Hu) - p.R8 * x(Ur);
%--------------------------------------------------------------------------
dydt(Rz) = p.R4prime * x(Ir) + p.R5 * x(Hr) + p.R5 * x(Hs);
dydt(Rx) = p.R9 * x(Cr) + p.R4 * x(Ix);
%--------------------------------------------------------------------------
dydt(D)  = p.R10  * x(Ud);
%--------------------------------------------------------------------------
end