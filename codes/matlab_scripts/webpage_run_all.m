% This script generates all the figures for the webpage
%--------------------------------
clc;
webpage_run_all_settings;
%--------------------------------
results_from_cpp_to_mat_federalstates;  webpage_run_all_settings;
webpage_CSV_file;                       webpage_run_all_settings;
webpage_rt_distribution;                webpage_run_all_settings;
webpage_federalStates;                  webpage_run_all_settings;
webpage_prediction;                     webpage_run_all_settings;
webpage_forecast_error;                 webpage_run_all_settings;