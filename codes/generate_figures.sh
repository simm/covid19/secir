#!/bin/bash
clear
SESSION=${1?Error: no session name is given.}
cd matlab_scripts
sed -i "/sessionName/c\sessionName = \""$1"\"" ./webpage_run_all_settings.m
matlab -nodisplay -nosplash -nodesktop -r "run ./webpage_run_all.m; exit;"