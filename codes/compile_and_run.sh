#!/bin/bash
clear
# The name of session shall be in the form of date of the final datapoint
# E.g. data published on 2021-01-04 (YYYY-MM-DD), the last datapoint belongs to 2021-01-03, and hence this is the name of the session
SESSION=${1?Error: no name given}

mkdir results
mkdir build
cd build
echo "cmake ..." 
cmake "Makefiles Unix" ../src
echo
echo "make  ..."
make
cd ..
cp ./build/SECIR .
echo "Run   ..."

cwd=$(pwd)
MODE="pool_run"
#---------------------------------
PARAMETER="./settings/param_random.csv"
#---------------------------------
## declare districts in array variable
declare -a arr=("Germany" "SchleswigHolstein" "Hamburg" "Niedersachsen" "Bremen" "NordrheinWestfalen" "Hessen" "RheinlandPfalz" "BadenWuerttemberg" "Bayern" "Saarland" "Berlin" "Brandenburg" "MecklenburgVorpommern" "Sachsen" "SachsenAnhalt" "Thueringen")
# Loop in districts
for i in "${arr[@]}"
do
   DISTRICT="$i"
   ./SECIR $SESSION $DISTRICT mode $MODE parameterFile $PARAMETER Population 83166711 mu 1 enable_time_varying_rho CFR_H 0.123768 CFR_L 0.007659 CFR_k 0.167916 CFR_t0 89.330331
	cd results/$SESSION/$DISTRICT
	zip results.zip *.csv
	rm *.csv
	cd $cwd   
done
#---------------------------------
./generate_figures.sh $SESSION