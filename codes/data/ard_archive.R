require(tidyverse)
require(ndjson)
require(broom)
require(git2r)
require(lubridate)
require(stringi)


pubdate     = "2021-01-24"
startingDate= "2020-01-27"
bydate      = "reporting"
groupby     = "Date"

Sys.setlocale(category = "LC_ALL", locale = "German")
districts = c("Germany",                "Schleswig-Holstein",     "Hamburg",          "Niedersachsen", "Bremen",                
              "Nordrhein-Westfalen",    "Hessen",                 "Rheinland-Pfalz",  "Baden-W�rttemberg",     
              "Bayern",                 "Saarland",               "Berlin",           "Brandenburg",           
              "Mecklenburg-Vorpommern", "Sachsen",                "Sachsen-Anhalt",   "Th�ringen")
# Filenames that are acceptible by c++
# Note that umlauts (�) and dashes are removed in filenames for C++
districts_saveName = c("Germany",                "SchleswigHolstein",     "Hamburg",          "Niedersachsen", "Bremen",                
                       "NordrheinWestfalen",    "Hessen",                 "RheinlandPfalz",  "BadenWuerttemberg",     
                       "Bayern",                 "Saarland",               "Berlin",           "Brandenburg",           
                       "MecklenburgVorpommern", "Sachsen",                "SachsenAnhalt",   "Thueringen")
#-----------------------------------------------------------
#  Set this true for the first time
#-----------------------------------------------------------
cloning = FALSE
#-----------------------------------------------------------
#  Clone data (for the first time only)
#-----------------------------------------------------------
if (cloning){
  clonePath = dirname(rstudioapi::getSourceEditorContext()$path)
  clonePath = paste(clonePath, "/RKI/", sep = "")
  clone(
    url = "https://github.com/ard-data/2020-rki-archive/",
    local_path = clonePath,
    bare = FALSE,
    branch = NULL,
    checkout = TRUE,
    credentials = NULL,
    progress = TRUE
  )
}

#-----------------------------------------------------------
#  Read file
#-----------------------------------------------------------
readndjson <- function(fn) {
  dest_file <- tempfile()
  inn <- xzfile(fn, open = "rb")
  out <- file(description = dest_file, open = "wb")
  nbytes <- 0
  repeat {
    bfr <- readBin(inn, what=raw(0L), size=1L, n=1e7)
    n <- length(bfr)
    if (n == 0L) break;
    nbytes <- nbytes + n
    writeBin(bfr, con=out, size=1L)
    bfr <- NULL  # Not needed anymore
  }
  close(inn)
  close(out)
  dat <- stream_in(dest_file)
  unlink(dest_file)
  dat
}
#-----------------------------------------------------------
#  Pull data
#-----------------------------------------------------------
repoPath = dirname(rstudioapi::getSourceEditorContext()$path)
repoPath = paste(repoPath, "/RKI/", sep = "")
git2r::pull(repoPath, credentials=NULL, merger=NULL)
#-----------------------------------------------------------
#  Find file
#-----------------------------------------------------------
#fn <- list.files(file.path(repoPath, "data/2_parsed"),
#                 pattern=sprintf("data_%s-\\d\\d-\\d\\d.ndjson.xz", pubdate),
#                 full.names=TRUE) %>%
#  ifelse(length(.)>1, tail(.,n=1), .) # use latest snapshot if there's more than one for a given date

fn <- list.files(file.path(repopath, "data/2_parsed"), 
                 pattern=sprintf("data_%s-\\d\\d-\\d\\d.ndjson.xz", pubdate),
                 full.names=TRUE) %>%
  ifelse(length(.)>1, tail(.,n=1), .) # use latest snapshot if there's more than one for a given date
#-----------------------------------------------------------
#  Find file
#-----------------------------------------------------------
#rawdata <- rjson::fromJSON(file=fn) %>% bind_rows
rawdata <- readndjson(fn) %>% bind_rows
get_RKI_data_method1 <- function(district, district_saveName, rawdata){
  #--------------------------------
  #  Filter data
  #--------------------------------
  if (district == "Germany") {
    dat <- rawdata %>% 
        mutate(Date=as_date(MeldedatumISO)) %>%
        group_by_at(groupby)  %>%
        filter(NeuerFall %in% c(0,1), as_date(MeldedatumISO) > as.Date(startingDate)) %>%
        summarize(I=sum(AnzahlFall), D=sum(AnzahlTodesfall))
  } else {
      dat <-rawdata %>% 
        mutate(Date=as_date(MeldedatumISO)) %>%
        group_by_at(groupby)  %>%
        filter(NeuerFall %in% c(0,1), as_date(MeldedatumISO) > as.Date(startingDate), Bundesland == district) %>%
        summarize(I=sum(AnzahlFall), D=sum(AnzahlTodesfall))  
  }
  # Some dates have no reports and therefore, are skipped in the dataset.
  # But they shall be given to the simulation code (c++). In the following, the missing dates are corrected.
  dates <- seq(as.Date("2020-01-01"), as.Date(pubdate), by=1)
  A = data.frame(date = dates, day_of_year = 1:length(dates), infected_newCases = 0, dead_newCases = 0)
  row.names(A) = dates
  A[as.character(dat$Date),3] = dat$I
  A[as.character(dat$Date),4] = dat$D
  
  A = mutate(A, infected_cumulative=cumsum(infected_newCases))
  A = mutate(A, dead_cumulative=cumsum(dead_newCases))
  
  A=A[-which(A$infected_cumulative == 0),]
  A=A[-nrow(A),]
  
  savePathDir = paste(dirname(rstudioapi::getSourceEditorContext()$path), "/", A$date[nrow(A)], sep = "")
  dir.create(savePathDir)
  write.csv(A,paste(savePathDir, "/", district_saveName,".csv", sep = ""), row.names = FALSE)
  A
}
#-----------------------------------------------------------
#  Main loop for all districts
#-----------------------------------------------------------
if (!is.na(fn) && length(fn)>0){
  for(i in 1:length(districts)) {
    dat = get_RKI_data_method1(districts[i], districts_saveName[i], rawdata)
  }
}