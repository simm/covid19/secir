# Results of COVID-19 SECIR model simulations for different geographic areas

This repository contains reports from model simulations of the SARS-CoV-2 pandemic.
The reports are regularly updated as the situation evolves.
The results are based on an Ordinary Differential Equation model developed by the [Systems Immunology team](http://www.theoretical-biology.de).

A **detailed description** of the model can be found in our article on medRxiv.

A **brief description** of the model can be found in this [Link](http://secir.theoretical-biology.de).

## Raw data of SECIR simulations:
To facilitate data exploration and visualizations, we provide daily updates of the historical and current reproduction numbers as CSV files.
It can be found in this [Link](https://gitlab.com/simm/covid19/secir/-/tree/master/img/dynamic/Rt_rawData).

**Please note that:**
1. Because of dynamic changes in the reported case numbers of the final days as the result of delayed reporting, calculating time-varying reproduction number ($`R_t`$) on those might be misleading. Therefore, we do not show the result for the two last days.

2. We always use the updated data from the Robert Koch Institute.
Due to delayed reporting, the data could change in different dates, even very old ones.
We run our algorithm every day from the starting date of the pandemic (occurance of the first incidence).
Therefore, our historical estimates of $`R_t`$ changes by updates in the data.

3. Our $`R_t`$ estimate is model-driven with parameters related to the disease progression and transmission.
The ranges of the parameters are informed by literature.
To account for uncertainties of the parameter values, we run our simulation multiple times, with random parameters sampling.
Therefore, we obtain a distribution of $`R_t`$ values for each date.
The results of simulations are given in different columns in the CSV files.

4. Due to code development, as well as running multiple computationally expensive simulations, a certain delay for updating daily reports shall be expected.

5. Outliers in our daily plots of $`R_t`$ distribution ([Link](http://secir.theoretical-biology.de)) are exluded.